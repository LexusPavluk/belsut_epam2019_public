package texts_and_strings;

import texts_and_strings.lingvunits.*;

// класс - фабрика лингвистических единиц - предложений, слов и пунктуации
public class TextUnitFactory {
    public IReadable newReadableObject(LinguisticUnitTypes type, CharSequence text) {
        switch (type) {
//            case TEXT:
//                return new Text(text);
            case SENTENCE:
                return new Sentence(text);
            case WORD_AND_SIGH:
                return new WordsOrSigns(text);
            default:
                throw new IllegalArgumentException("Invalid text type:" + type);
        }
    }
}
