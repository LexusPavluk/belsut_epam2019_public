package texts_and_strings;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class TextFileReader {
    private static final String textsPath = "resources\\The_Hobbit.txt";

    private String text;

    public String getText() {
        return text;
    }

    public TextFileReader() throws IOException {
        this.text = textFromFile();
    }

    private static String textFromFile() throws IOException {
        List<String> textFromFile = Files.readAllLines(Paths.get(textsPath), StandardCharsets.UTF_8);
        StringBuilder tempString = new StringBuilder();
        for (String elem : textFromFile) {
            tempString.append(elem);
        }
        return tempString.toString();
    }

}
