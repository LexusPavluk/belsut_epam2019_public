package texts_and_strings.lingvunits;

import texts_and_strings.TextUnitFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
* класс абстрактной Лингвистической единицы.
*
*/
public abstract class LingUnit implements IReadable {
    protected LinguisticUnitTypes typeSubUnit;
    protected List<IReadable> record;

    // возвращает длину записи лингвистических единиц
    @Override
    public int getLength() {
        return record.size();
    }

    // возвращает запись лингвистической единицы
    @Override
    public List<IReadable> getRecord() {
        return record;
    }

    //сохраняет себя в список record в виде лингвистических единиц своего подтипа
    @Override
    public List<IReadable> stringToRecord(CharSequence stringToRec) {
        List<String> tempList = explodeString(stringToRec, typeSubUnit);
        ArrayList<IReadable> list = new ArrayList<>();
        TextUnitFactory factory = new TextUnitFactory();
        for (String elem : tempList) {
            list.add(factory.newReadableObject(typeSubUnit, elem));
        }
        return list;
    }


    //  разбивает себя на лингвистические подтипы typeSubUnit
    protected List<String> explodeString(CharSequence explodedString,
                                                LinguisticUnitTypes typeSubUnit) {
        Pattern pattern = Pattern.compile(typeSubUnit.getRegexValue());
        Matcher matcher = pattern.matcher(explodedString);
        List<String> tempList = new ArrayList<>();
        while (matcher.find()) {
            tempList.add(matcher.group());
        }
        return tempList;
    }



}
