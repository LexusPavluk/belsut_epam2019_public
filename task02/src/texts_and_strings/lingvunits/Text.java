package texts_and_strings.lingvunits;

import java.util.Objects;
/* класс - Текста (наследуемый от лигвистической единицы)*/
public class Text extends LingUnit {

    public Text() {
        this.typeSubUnit = LinguisticUnitTypes.SENTENCE; // подтип текста - предложение
    }

    public Text(CharSequence text) {
        this();
        this.record = stringToRecord(text);
    }

    // метод получения предложения из текста по индексу
    public Sentence getSentence(int index) {
        return (Sentence) record.get(index);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (IReadable obj : record) {
            builder.append(obj.toString());
            builder.append(" ");
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Text)) return false;
        Text text = (Text) o;
        return typeSubUnit == text.typeSubUnit &&
                getRecord().equals(text.getRecord());
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeSubUnit, getRecord());
    }
}
