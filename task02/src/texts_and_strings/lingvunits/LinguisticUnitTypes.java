package texts_and_strings.lingvunits;

/*класс - перечисление типов лингвистических единиц
с соответствующими им RegEX-ами (для разбивки на подтипы).
* */
public enum LinguisticUnitTypes {
    TEXT("(.|\\n)+"),
    SENTENCE("([A-Z])[^!\\.\\?]+([(\\.\\s)|(\\?\\s)|(!\\s)])"), // ([A-Z])[^!\.\?]+([(\.\s)|(\?\s)|(!\s)])
    WORD_AND_SIGH("((\\w+[-’]?)+|[.,!?;:—()])");   // ((\w+[-’]?)+|[.,!?:—])
    //ANY_SINGLE_SIGN("(.{1})");        //  (.{1})

    private String regexValue;

    LinguisticUnitTypes(String regex) {
        this.regexValue = regex;
    }

    public String getRegexValue() {
        return regexValue;
    }
}
