package texts_and_strings.lingvunits;

import java.util.List;
import java.util.Objects;

/*
 * класс - "Слова и пунктуация" (наследуемый от лигвистической единицы) */

public class WordsOrSigns extends LingUnit {
    protected String wordOrSign;

    public WordsOrSigns(CharSequence word) {
        this.wordOrSign = word.toString();
    }

    // удаляем все последующие вхождения первой буквы в слове
    public void removeAllFollowingInstanceOfFirstLetter() {
        String firstLetter = String.valueOf(wordOrSign.charAt(0));
        String processedWord = toString().toLowerCase();
        StringBuilder temp = new StringBuilder(processedWord.replace(firstLetter.toLowerCase(), ""));
        temp.insert(0, firstLetter);
        wordOrSign = temp.toString();
    }


    @Override
    public String toString() {
        return wordOrSign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WordsOrSigns)) return false;
        WordsOrSigns that = (WordsOrSigns) o;
        return wordOrSign.equals(that.wordOrSign);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wordOrSign);
    }


    @Override
    public List<IReadable> stringToRecord(CharSequence explodedString) {
        return null;
    }

    @Override
    public int getLength() {
        return 0;
    }

    @Override
    public List<IReadable> getRecord() {
        return null;
    }
}
