package texts_and_strings.lingvunits;

import java.util.List;

/*
* Интерфейс читаемой сущности - можно записать, получить её длину, прочитать
* */
public interface IReadable {

    List<?> stringToRecord(CharSequence explodedString);

    int getLength();

    List<?> getRecord();


}
