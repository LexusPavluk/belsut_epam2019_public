package texts_and_strings.lingvunits;

import java.util.Objects;
import java.util.regex.Pattern;
import static java.lang.String.valueOf;
/*
* класс - Предложения (наследуемый от лигвистической единицы) */
public class Sentence extends LingUnit {
    private Type type; // тип предложения: ". ! ?"

    public Sentence() {
        this.typeSubUnit = LinguisticUnitTypes.WORD_AND_SIGH;
    }

    public Sentence(CharSequence sentence) {
            this();
        this.record = stringToRecord(sentence);
        this.type = detectSentenceType(sentence);
    }

    // определение вида предложения по его последнему знаку препинания
    protected Type detectSentenceType(CharSequence sentence) {
        String lastSign = valueOf(sentence.charAt(sentence.length() - 1));
        switch (lastSign) {
            case "!":
                return Type.EXCLAMATION;
            case "?":
                return Type.INTERROGATIVE;
            default:
                return Type.DECLARATIVE;
        }
    }


/*
    public WordsOrSigns getWord(int index) {
        return (WordsOrSigns) record.get(index);
    }

    public List<IReadable> listUniqueWords() {
        List<IReadable> temp = new ArrayList<>();
        temp.add(record.get(0));
        List<IReadable> returnedList = new ArrayList<>();
        for (IReadable word : temp) {
            if (!returnedList.contains(word)) {
                returnedList.add(word);
            }
        }
        return returnedList;
    }
*/
    // меняем местами слова в предложении
    public void exchangeWordByPosition(int firstPosition,
                                       int secondPosition) {
        WordsOrSigns temp = (WordsOrSigns) record.get(secondPosition);
        record.set(secondPosition, record.get(firstPosition));
        record.set(firstPosition, temp);
    }

    // удаляем подстроку в предложении от указанной первой буквы до указанной последней
    public void removeSubstringByCharacter(String startingChar, String finalChar) {
        int startIndex = toString().indexOf(startingChar);
        int finalIndex = toString().lastIndexOf(finalChar);
        if (startIndex > 0 && startIndex < finalIndex) {
            StringBuilder newSentence = new StringBuilder(toString());
            newSentence.replace(startIndex, finalIndex, "");
            this.record = stringToRecord(newSentence);
        }
    }

    // преобразование списка "слов и пунктуации" в строку
    private String recordToString() {
        StringBuilder builder = new StringBuilder();
        for (IReadable obj : record) {
            if (Pattern.matches("([.,:;!?)']){1}", obj.toString())) {  // withoutPreSpace
                builder.append("\b");
            }
            builder.append(obj.toString());
            if (!Pattern.matches("([.!?(]{1})", obj.toString())) {  // withoutPostSpace
                builder.append(" ");
            }
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return recordToString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sentence)) return false;
        Sentence sentence = (Sentence) o;
        return type == sentence.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type);
    }

    // внутренний класс - перечисление видов предложения - повествовательное, восклицательное, вопросительное
    public enum Type {
        DECLARATIVE("."),
        EXCLAMATION("!"),
        INTERROGATIVE("?");

        private String typeValue;

        Type(String type) {
            this.typeValue = type;
        }

        public String getTypeValue() {
            return typeValue;
        }

    }

}
