package texts_and_strings;

import texts_and_strings.lingvunits.IReadable;
import texts_and_strings.lingvunits.Sentence;
import texts_and_strings.lingvunits.Text;
import texts_and_strings.lingvunits.WordsOrSigns;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;


public class Runner {
    public static void main(String[] args) throws IOException {

        TextFileReader reader = new TextFileReader(); // читаем текст из файла

        Text text = new Text(reader.getText());  // передаём его в класс "Текст"
        System.out.println(text);

        // удаляем все последующие вхождения первой буквы каждого слова в вопросительных предложениях
        removeLettersLikeFirstInSentences(text, Sentence.Type.INTERROGATIVE);
        // в консоль все слова первого предложения отсутствующие в последующих
        System.out.println(findWordsMissingInOtherSentences(text));
        // меняем первое слово с последним в восклицательных предложениях
        changeFirstByLastWord(text, Sentence.Type.EXCLAMATION);
        // удаляем подстроку максимальной длины от первого указанного символа до последнего указанного символа
        // в повествовательных предложениях
        removeMaxSubstringByCharacter(text, Sentence.Type.DECLARATIVE, 'g', 't');

        System.out.println(text);

    }

    // Во всех вопросительных предложениях текста преобразовать каждое слово,
    // удалив из него все последующие вхождения первой буквы этого слова.
    private static void removeLettersLikeFirstInSentences(Text someText,
                                                          Sentence.Type type) {
        for (int i = 0; i < someText.getLength(); i++) {
            Sentence sentence = (Sentence) someText.getRecord().get(i);
            if (sentence.toString().endsWith(type.getTypeValue())) {
                for (int j = 0; j < sentence.getLength(); j++) {
                    WordsOrSigns word = (WordsOrSigns) sentence.getRecord().get(j);
                    word.removeAllFollowingInstanceOfFirstLetter();
                }
            }
        }
    }

    //Найти все слова в первом предложении, которых нет ни в одном из остальных предложений.
    private static List<IReadable> findWordsMissingInOtherSentences(Text someText) {
        Sentence representedSentence = (Sentence) someText.getRecord().get(0);
        List<IReadable> returnedList = new ArrayList<>(representedSentence.getRecord());
            for (int j = 1; j < someText.getLength(); j++) {
                List<IReadable> analyzableSentence = someText.getSentence(j).getRecord();
                returnedList.removeAll(analyzableSentence);
        }
        return returnedList;
    }

    // В каждом восклицательном предложении текста поменять местами первое слово с последним.

    private static void changeFirstByLastWord(Text someText, Sentence.Type type) {
        List<IReadable> textList = someText.getRecord();
        for (int i = 0; i < someText.getLength(); i++) {
            Sentence sentence = (Sentence) textList.get(i);
            if (sentence.toString().endsWith(type.getTypeValue())) {
                sentence.exchangeWordByPosition(0, sentence.getLength() - 2);
            }
        }
    }

    //В каждом повествовательном предложении текста исключить подстроку максимальной длины,
    // начинающуюся и заканчивающуюся заданными символами.
    private static void removeMaxSubstringByCharacter(Text someText,
                                                      Sentence.Type type,
                                                      char firstChar, char secondChar) {
        List<IReadable> textList = someText.getRecord();
        for (int i = 0; i < someText.getLength(); i++) {
            Sentence sentence = (Sentence) textList.get(i);
            if (sentence.toString().endsWith(type.getTypeValue())) {
                sentence.removeSubstringByCharacter(valueOf(firstChar), valueOf(secondChar));
            }
        }
    }
}
