package serialization;

import serialization.box.SheetBox;
import serialization.figure.film.*;
import serialization.figure.paper.*;
import serialization.figure.paper.IPaintable.*;

import java.io.InvalidObjectException;

/**
 * Point of entry to class and application - use to run demonstration and test the code.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 2.0
 */

public class Main {

    public static void main(String[] args) {

        SheetBox playBox = new SheetBox();  //create box girl's

        // create and add figures to box
        IFilm filmCircle13 = new FilmCircle(13.5);
        IFilm filmTriangle15 = new FilmTriangle(15.3);
        IFilm filmRectangle12_25 = new FilmRectangle(12.5, 25.0);
        playBox.addFigure(filmCircle13, filmTriangle15, filmRectangle12_25);

        IFilm filmRectangle25_15 = new FilmRectangle(25.2, 15.7);
        IFilm filmCircle_7 = new FilmCircle(7.2, filmRectangle25_15);
        playBox.addFigure(filmCircle_7);

        IPaper paperCircle15 = new PaperCircle(15.0);
        paperCircle15.setColorSheet(Palette.ROSE);
        //paperCircle15.setColorSheet(Palette.BLACK); // try painting twice
        playBox.addFigure(paperCircle15);

        IPaper paperTriangle5 = new PaperTriangle(5.1);
        paperTriangle5.setColorSheet(Palette.RED);

        IPaper paperTriangle3 = new PaperTriangle(3.3);
        paperTriangle3.setColorSheet(Palette.BLUE);

        IPaper paperCircle6 = new PaperCircle(6.8);
        paperCircle6.getColorSheet();

        IPaper paperCircle = new PaperCircle(4.2);
        paperCircle.setColorSheet(Palette.BLACK);
        playBox.addFigure(paperTriangle5, paperTriangle3, paperCircle6, paperCircle);
        //playBox.addFigure(paperCircle15); // trying to add same figure to Box

        IPaper paperTriangle19 = new PaperTriangle(19.6);
        paperTriangle19.setColorSheet(Palette.VIOLET);
        IPaper paperRectangle6_4 = new PaperRectangle(6.2, 4.3, paperTriangle19);
        playBox.addFigure(paperRectangle6_4);

        playBox.getCountFigures(); // work with box
        playBox.getByNumber(5);
        playBox.removeByNumber(5);
        IFilm filmRectangle7_3 = new FilmRectangle(7.3, 3.2);
        playBox.changeByNumber(filmRectangle7_3, 6);
        playBox.indexPerSample(paperTriangle5);
        playBox.getTotalArea();
        playBox.totalPerimeter();
        System.out.println(playBox);

        String fileName = "resources\\box.data";

        Serializator serializator = new Serializator();
        boolean bool = false;
        try {
            bool = serializator.serialization(playBox, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(bool);

        //fileName = "resources\\box1.data";
        SheetBox otherGirLBox = null;
        try {
            otherGirLBox = serializator.deserialization(fileName);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }
        System.out.println(otherGirLBox);
    }
}