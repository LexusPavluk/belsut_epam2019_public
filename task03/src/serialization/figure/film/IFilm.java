package serialization.figure.film;

import serialization.entities.ISheet;
import serialization.entities.ITransparentable;

/**
 * Marker interface of Film.
 * The Film is transparent, so painting methods are no available.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */


public interface IFilm extends ISheet, ITransparentable {
}
