package serialization.figure.film;

import serialization.figure.*;

/**
 * FilmCircle class - extend abstract class <tt>Circle</tt>.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public class FilmCircle extends Circle implements IFilm {

    /**
     * Constructor of class FilmCircle with specified radius.
     *
     * @param radius radius for making FilmCircle
     */
    public FilmCircle(double radius) {
        super(radius);
    }

    /**
     * Constructor of class FilmCircle with specified radius, and the <tt>Figure</tt> from which it is cut.
     *
     * @param radius radius for making <tt>FilmCircle</tt>.
     * @param figure <tt>Figure</tt> which cut.
     */
    public FilmCircle(double radius, IFilm figure) {
        super(radius, (Figure) figure);
    }

}
