package serialization.figure;

import static java.lang.Math.*;

/**
 * Static utils class. Provides check correct data input, and possibility cutout figures from other.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public final class ValidationUtils {

    /**
     * Don't let anyone instantiate this class.
     */
    private ValidationUtils() {
    }

    /**
     * Checking input data of correct values. Size must be positive.
     *
     * @param validationValue value of the dimension.
     */
    public static void verificationPositivityNumber(double validationValue) {
        if (validationValue <= 0) {
            throw new IllegalArgumentException("The dimension must be greater than zero");
        }
    }

    /**
     * The {@code String} value output in console
     * <i>message</i>, if check of possibility cutout figure from another is not performed.
     */
    private static final String MESSAGE = "The cutting figure dimensions are incorrect";

    /**
     * Checking possibilities of cutout Figure from another. Basing on <tt>Figure.getDefiningSize()</tt> value.
     * Defining size of base figure must by greater than of some particular value cutout Figure. For each type of
     * Figure is his own certain value. For example cutout <tt>Circle</tt> passes to the method radius, but
     * <tt>Rectangle</tt> passes both of its size.
     *
     * @param figure from which it is cut.
     * @param size   value of size.
     * @see Figure#getDefiningSize .
     */
    public static void checkPossibilityMakeFigure(Figure figure, double size) {
        if (figure.getDefiningSize() <= size) {
            throw new IllegalArgumentException(MESSAGE);
        }
    }

    /**
     * Checking possibilities of cutout Figure from another. Basing on <tt>Figure.getDefiningSize()</tt> value.
     * Defining size of base figure must by greater than of some particular value cutout Figure. For each type of
     * Figure is his own certain value. For example cutout <tt>Circle</tt> passes to the method radius, but
     * <tt>Rectangle</tt> passes both of its size.
     *
     * @param figure from which it is cut.
     * @param size1  value of size.
     * @param size2  value of size.
     * @see Figure#getDefiningSize .
     */
    public static void checkPossibilityMakeFigure(Figure figure, double size1, double size2) {
        if (figure instanceof Rectangle) {
            Rectangle rectangle = (Rectangle) figure;
            double minSide = min(rectangle.getSide1(), rectangle.getSide2());
            double maxSide = max(rectangle.getSide1(), rectangle.getSide2());
            if (minSide <= min(size1, size2) | maxSide <= max(size1, size2)) {
                throw new IllegalArgumentException(MESSAGE);
            }
        } else {
            checkPossibilityMakeFigure(figure, min(size1, size2));
        }
    }


}

