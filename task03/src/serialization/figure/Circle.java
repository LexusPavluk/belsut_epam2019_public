package serialization.figure;

import java.io.Serializable;
import java.util.Objects;
import static java.lang.Math.*;

/**
 * Abstract class of Circle - implements methods of abstract class <tt> Figure </tt>, from which extended.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public abstract class Circle extends Figure {

    /**
     * The {@code double} value of
     * <i>RADIUS</i> from Circle.
     */
    protected final double radius;

    /**
     * Constructor of class Circle with specified radius.
     *
     * @param radius radius for making Circle
     */
    public Circle(double radius) {
        ValidationUtils.verificationPositivityNumber(radius);
        this.radius = radius;
    }

    /**
     * Constructor of class Circle with specified radius, and the <tt>Figure</tt> from which it is cut.
     *
     * @param radius radius for making Circle.
     * @param figure Figure which cut.
     */
    public Circle(double radius, Figure figure) {
        ValidationUtils.verificationPositivityNumber(radius);
        ValidationUtils.checkPossibilityMakeFigure(figure, radius);
        this.radius = radius;
    }

    /**
     * Getter for radius of Circle.
     *
     * @return RADIUS radius of Circle.
     */
    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return PI * pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * PI * radius;
    }

    @Override
    public double getDefiningSize() {
        return radius;
    }

    @Override
    public String toString() {
        return super.toString() + " " + radius;
    }

/*
    @Override
    public boolean equals(Object obj) {
        if(super.equals(obj)) {
            Circle other = (Circle) obj;
            if(RADIUS != other.getRADIUS()) return false;
            else return true;
        } else return false;
    }
*/

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            Circle other = (Circle) obj;
            return Double.compare(radius, other.getRadius()) == 0;
        } else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), radius);
    }
}


