package serialization.figure.paper;

import serialization.figure.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Paper Rectangle class - extend abstract class <tt>Rectangle</tt>.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public class PaperRectangle extends Rectangle implements IPaper {

    /**
     * The {@code Colors} value of
     * <i>paperColor</i> from <tt>PaperRectangle</tt>. Field paperColor is reference in nested <tt>Color</tt> object,
     * which stores value of paper color.
     */
    private IPaintable.Painted paperColor = new Painted();

    /**
     * Constructor of class PaperRectangle with specified sides.
     *
     * @param side1 first side for making FilmRectangle.
     * @param side2 second side for making FilmRectangle.
     */
    public PaperRectangle(double side1, double side2) {
        super(side1, side2);
    }

    /**
     * Constructor of class PaperRectangle with specified sides, and the <tt>Figure</tt> from which it is cut.
     *
     * @param side1  first side for making FilmRectangle.
     * @param side2  second side for making FilmRectangle.
     * @param figure Figure which cut.
     */
    public PaperRectangle(double side1, double side2, IPaper figure) {
        super(side1, side2, (Figure) figure);
        this.paperColor.setColor(figure.getColorSheet());
    }

    @Override
    public Palette getColorSheet() {
        return paperColor.getColor();
    }

    @Override
    public void setColorSheet(Palette newColor) {
        this.paperColor.setColor(newColor);
    }


    @Override
    public String toString() {
        return super.toString() + " " + getColorSheet();
    }

    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            PaperTriangle other = (PaperTriangle) obj;
            return getColorSheet() == other.getColorSheet();
        } else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), paperColor);
    }

}
