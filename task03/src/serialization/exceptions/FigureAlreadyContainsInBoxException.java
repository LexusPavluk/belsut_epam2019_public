package serialization.exceptions;

public class FigureAlreadyContainsInBoxException extends RuntimeException {

    public FigureAlreadyContainsInBoxException() {
    }

    public FigureAlreadyContainsInBoxException(String message) {
        super(message);
    }
}
