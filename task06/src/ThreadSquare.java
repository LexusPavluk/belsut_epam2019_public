import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by vcnuv on 07.06.2018.
 */
public class ThreadSquare implements Runnable {
    private final boolean horiz;
    private int x, y;
    private int direction = 1;
    JButton button;
    private final Container contentPane;
    static TrafficLights trafficLights;

    public ThreadSquare(boolean horiz, TrafficLights trafficLights) {
        this.horiz = horiz;
        ThreadSquare.trafficLights = trafficLights;
        if (horiz) {
            x = 0;
            y = Settings.POINT_CROSS_ROAD.y + (int) (Math.random() *
                    (Settings.DIMENSION_CROSS_ROAD.height - Settings.SQUARE_SIDE));
        } else {
            x = Settings.POINT_CROSS_ROAD.x + (int) (Math.random() *
                    (Settings.DIMENSION_CROSS_ROAD.width - Settings.SQUARE_SIDE));
            y = 0;
        }
        this.contentPane = Settings.contentPane;
        initButton();
    }

    @Override
    public void run() {
        int age = 0;
        while (true /*age < 10000*/) {
            while (!trafficLights.isBeforeCrossRoad(direction, x, y)) {
                move();
                getNextPoint();
            }
            moveOnCrossroads();
//            ++age;
        }
        // kill Thread there!!!
    }

    synchronized void moveOnCrossroads() {
/*
        Thread backDaemon = new Thread() {
            boolean isOnCrossroad = false;

            public void run() {
                while (tf.isOnCrossRoad(x, y)) {
                    this.isOnCrossroad = tf.isOnCrossRoad(x, y);
                }
                this.interrupt();
            }
        };

        backDaemon.setDaemon(true);
        backDaemon.start();

 */
        while (!trafficLights.isCrossroadFree(button)) {
        }
        trafficLights.setBusyCrossroad(button);
        while (trafficLights.isOnCrossRoad(x, y) |
                trafficLights.isBeforeCrossRoad(direction, x, y)) {
            move();
            getNextPoint();
        }
        trafficLights.setCrossroadFree();
    }

    private int getDirection() {
        if (x <= 0 || y <= 0) direction = 1;                                    // граничные условия
        if ((x >= Settings.FRAME_WIDTH - Settings.SQUARE_SIDE) ||
                (y >= Settings.FRAME_HEIGHT - Settings.SQUARE_SIDE * 2.5)) {
            direction = -1;
        }                                                                          // иные условия

        return direction;
    }

    private void getNextPoint() {
        int nextStep = Settings.STEP_SQUARE * getDirection();
        if (horiz) {
            x += nextStep;
        } else {
            y += nextStep;
        }
    }

    private void move() {
        button.setBounds(x, y, Settings.SQUARE_SIDE, Settings.SQUARE_SIDE);
        try {
            Thread.sleep(Settings.DELAY_SQUARE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initButton() {
        Random random = new Random();
        Color[] color = {new Color(0, 0, 0),
                new Color(255, 0, 0),
                new Color(255, 127, 0),
                new Color(255, 255, 0),
                new Color(0, 255, 118),
                new Color(0, 255, 255),
                new Color(0, 0, 255),
                new Color(255, 0, 255)};
        button = new JButton("" + (++CrossRoad.counter));
        button.setLocation(x, y);
        button.setSize(Settings.SQUARE_SIDE, Settings.SQUARE_SIDE);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setForeground(Settings.COLOR_TEXT_SQUARE);
        button.setBackground(color[random.nextInt(color.length)]);
        contentPane.add(button);
    }
}
