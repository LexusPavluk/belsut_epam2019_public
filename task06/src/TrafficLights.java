import javax.swing.*;
import java.awt.*;

public class TrafficLights extends Thread {
    private Color crossroadColor = Color.GREEN;
    private static int countButton;
    static final int X_FORWARD_BORDER_CROSSROAD =
            Settings.POINT_CROSS_ROAD.x - Settings.SQUARE_SIDE;
    static final int X_BACKWARD_BORDER_CROSSROAD =
            Settings.POINT_CROSS_ROAD.x + Settings.DIMENSION_CROSS_ROAD.width;
    static final int Y_FORWARD_BORDER_CROSSROAD =
            Settings.POINT_CROSS_ROAD.y - Settings.SQUARE_SIDE;
    static final int Y_BACKWARD_BORDER_CROSSROAD =
            Settings.POINT_CROSS_ROAD.y + Settings.DIMENSION_CROSS_ROAD.height;

    synchronized public boolean isCrossroadFree(JButton button) {
        boolean free = countButton == 0 ||
                (!CrossRoad.firstTask && (button.getBackground().equals(crossroadColor)));
        if (!free) {
            try {
                wait((long) (137 * Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return free;
    }

    synchronized void setBusyCrossroad(JButton button) {
        crossroadColor = button.getBackground();
        ++countButton;
        if (!CrossRoad.firstTask) notifyAll();
    }

    synchronized void setCrossroadFree() {
        --countButton;
        if (CrossRoad.firstTask) {
            notify();
        } else notifyAll();
    }

    synchronized boolean isOnCrossRoad(int x, int y) {
        return (x >= X_FORWARD_BORDER_CROSSROAD && x <= X_BACKWARD_BORDER_CROSSROAD) &&
                (y >= Y_FORWARD_BORDER_CROSSROAD && y <= Y_BACKWARD_BORDER_CROSSROAD);
    }

    boolean isBeforeCrossRoad(int direction, int x, int y) {
        boolean isBefore;
        if (direction > 0) {
            isBefore = ((x > X_FORWARD_BORDER_CROSSROAD - 1 && x <= X_FORWARD_BORDER_CROSSROAD) |
                    (y > Y_FORWARD_BORDER_CROSSROAD - 1 && y <= Y_FORWARD_BORDER_CROSSROAD));
        } else {
            isBefore = ((x >= X_BACKWARD_BORDER_CROSSROAD && x < X_BACKWARD_BORDER_CROSSROAD + 1) |
                    (y >= Y_BACKWARD_BORDER_CROSSROAD && y < Y_BACKWARD_BORDER_CROSSROAD + 1));
        }
        return isBefore;
    }

}
