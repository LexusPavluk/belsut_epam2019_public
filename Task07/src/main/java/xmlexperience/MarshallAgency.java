package xmlexperience;

import xmlexperience.generated.Agency;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class MarshallAgency {

    public void marshalliseTouristVoucher(Agency agency) {
        try {
            JAXBContext context = JAXBContext.newInstance(Agency.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(agency, new FileOutputStream("src/main/java/xmlexperience/Agency_marsh.xml"));
            m.marshal(agency, System.out);
        } catch (JAXBException e) {
            System.err.println("XML-file does not may be created:" + e);;
        } catch (FileNotFoundException e) {
            System.err.println("JAXB-context is incorrect:" + e);;
        }
    }
}
