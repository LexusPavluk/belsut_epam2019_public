package xmlexperience.agency;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TouristVoucher", propOrder = {
        "type",
        "country",
        "numberNights",
        "transport",
        "hotelCharacteristic",
        "cost"
})

public class TouristVoucher {
    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    private String name;
    @XmlElement(required = true)
    private TypeTour type;
    @XmlElement(required = true, name = "country")
    private List<Country> country = new ArrayList<Country>();
    @XmlElement(required = true)
    private int numberNights;
    @XmlElement(required = true)
    private Transport transport;
    @XmlElement(required = true)
    private Hotel hotelCharacteristic = new Hotel();
    @XmlElement(required = true)
    private int cost;

    public TouristVoucher(){}
    public TouristVoucher(String name, String type, int numberNights,
                          String transport, Hotel hotel,
                          int cost, String... countries) {
        this.name = name;
        this.type = TypeTour.valueOf(type.toUpperCase().replace(' ','_'));
        setCountry(countries);
        this.numberNights = numberNights;
        this.transport = Transport.valueOf(transport.toUpperCase());
        this.hotelCharacteristic = hotel;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeTour getType() {
        return type;
    }

    public void setType(TypeTour type) {
        this.type = type;
    }

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(String... countries) {
        List<Country> countriesList = new ArrayList<Country>();
        for (String country: countries) {
            countriesList.add(Country.valueOf(country.toUpperCase().replace(' ','_')));
        }
        this.country = countriesList;
    }

    public int getNumberNights() {
        return numberNights;
    }

    public void setNumberNights(int numberNights) {
        this.numberNights = numberNights;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Hotel getHotelCharacteristic() {
        return hotelCharacteristic;
    }

    public void setHotelCharacteristic(Hotel hotelCharacteristic) {
        this.hotelCharacteristic = hotelCharacteristic;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }


    @Override
    public String toString() {
        return "\nTourist Voucher{" + "name= " +  name +
                ", type=" + type + ", countries" + country +
                ", numberNights=" + numberNights + ", transport=" + transport +
                ", " + hotelCharacteristic + ", сost=" + cost +"}";
    }

    @XmlType
    @XmlEnum()
    enum TypeTour {
        WEEKEND_TOUR,
        REST,
        FAMILY_REST,
        EXCURSION,
        PILGRIMAGE,
        SPA_REST
    }

    @XmlType
    @XmlEnum()
    enum Country {
        BELARUS,
        UKRAINE,
        RUSSIAN_FEDERATION,
        TURKEY,
        FRANCE,
        GERMANY,
        IRELAND,
        NETHERLANDS,
        CZECHIA,
        SWEDEN,
        MARSHALL_ISLANDS

    }

    @XmlType
    @XmlEnum()
    enum Transport {
        AVIA,
        AUTO,
        BUS,
        RAILWAY,
        SHIP
    }


}
