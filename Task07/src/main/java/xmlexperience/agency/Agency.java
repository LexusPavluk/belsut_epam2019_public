package xmlexperience.agency;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Agency {
    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    String name = "Your new tour operator!";
    @XmlElement(name = "TouristVoucher",required = true)
    private List<TouristVoucher> touristVoucher = new ArrayList<TouristVoucher>();

    public Agency() {
        super();
    }

    public void setTouristVoucher(ArrayList<TouristVoucher> touristVoucher) {
        this.touristVoucher = touristVoucher;
    }

    public boolean addVoucher(TouristVoucher... tvs) {
        boolean added = true;
        for (TouristVoucher elem: tvs) {
            added = added & touristVoucher.add(elem);
        }
        return added;
    }

    @Override
    public String toString() {
        return "Agency [Tourist voucher list=" + touristVoucher + "\n]";
    }
}
