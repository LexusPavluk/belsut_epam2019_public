package xmlexperience.agency;

import javax.xml.bind.annotation.*;
import java.util.Arrays;
import java.util.List;
@XmlRootElement
@XmlType(name = "hotelCharacteristic", propOrder = {
        "starRating",
        "nutrition",
        "rooms",
        "amenities"
})
public class Hotel {
    int starRating;
    Nutrition nutrition;
    int rooms;
    List<String> amenities;

    public Hotel() {
    }
    public Hotel(int starRating,
                 String nutrition,
                 int rooms,
                 String... amenities) {
        setStarRating(starRating);
        this.nutrition = Nutrition.valueOf(nutrition.toUpperCase());
        setRooms(rooms);
        this.amenities = Arrays.asList(amenities);
    }

    public int getStarRating() {
        return starRating;
    }

    @XmlElement(required = true)
    public void setStarRating(int starRating) {
        this.starRating = (starRating > 0 && starRating < 6) ? starRating : 5;
    }

    public Nutrition getNutrition() {
        return nutrition;
    }

    @XmlElement(required = true)
    public void setNutrition(Nutrition nutrition) {
        this.nutrition = nutrition;
    }

    public int getRooms() {
        return rooms;
    }

    @XmlElement(required = true)
    public void setRooms(int rooms) {
        this.rooms = (rooms > 0 && rooms < 4) ? rooms : 3;
    }

    public List<String> getAmenities() {
        return amenities;
    }

    @XmlElement
    public void setAmenities(List<String> amenities) {
        this.amenities = amenities;
    }

    @Override
    public String toString() {
        return "hotelCharacteristic: starRating=" + starRating +
                ", nutrition=" + nutrition + ", rooms=" + rooms +
                ", amenities" + amenities;
    }

    @XmlType
    @XmlEnum()
    enum Nutrition {
        RO, BB, HB, FB, AI, UAI
    }
}
