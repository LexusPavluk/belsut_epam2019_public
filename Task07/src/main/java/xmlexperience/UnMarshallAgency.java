package xmlexperience;

import org.xml.sax.SAXException;
import xmlexperience.generated.Agency;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class UnMarshallAgency {

    public Agency unMarshalliseTouristVoucher() {
        Agency agency = null;
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance("xmlexperience.generated");
            Unmarshaller um = context.createUnmarshaller();
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            File schemaLocation = new File("src/main/java/xmlexperience/Agency.xsd");
            Schema schema = factory.newSchema(schemaLocation);
            um.setSchema(schema);
            agency = (Agency) um.unmarshal(new File("src/main/java/xmlexperience/Agency.xml"));
        } catch (JAXBException e) {
            System.err.println("XML-file does not may be created:" + e);
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return agency;
    }
}
