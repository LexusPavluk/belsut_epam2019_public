package xmlexperience;
/*
1) индивидуально по заданию
написать (вручную  будет более полезно) xsd схему (сложные типы, перечисления и т.п.)
сгенерировать java классы по схеме
создать несколько объектов и преобразовать (marshal) их в  xml
провести валидацию xml
*/

//import xmlexperience.agency.Agency;
//import xmlexperience.agency.Hotel;
//import xmlexperience.agency.TouristVoucher;
import xmlexperience.generated.Agency;
import xmlexperience.validator.ValidatorSAX;

public class Main {

    public static void main(String[] args) {

/*        Hotel hc1 = new Hotel(3, "BB",
                1, "Bar", "Conditioner", "Gym");
        TouristVoucher tv1 = new TouristVoucher("In Ukraine whiz love","Weekend_tour",
                2, "Bus", hc1, 100,
                "Ukraine");

        Hotel hc2 = new Hotel(4, "HB",
                2, "Childcare", "Kitchen", "Restaurant","Bathtub in room",
                "Pool","Parking available","Airport transfers");
        TouristVoucher tv2 = new TouristVoucher("Legends of Czech beer","Rest",
                6, "Avia", hc2, 600,
                "Czechia");

        Hotel hc3 = new Hotel(5, "AI",
                3, "Free parking", "Free Wi-FI", "Ski storage","Gym",
                "Car charging point");
        TouristVoucher tv3 = new TouristVoucher("Scandinavia fjords","Excursion",
                13, "Auto", hc3, 1000,
                "Sweden", "Ireland");
        Agency agency = new Agency();
        agency.addVoucher(tv1, tv2, tv3);*/


        Agency agency = new UnMarshallAgency().unMarshalliseTouristVoucher();

        System.out.println(agency);
        
        MarshallAgency mag = new MarshallAgency();
        mag.marshalliseTouristVoucher(agency);

        ValidatorSAX vSAX = new ValidatorSAX();
        vSAX.validation();



    }
}
