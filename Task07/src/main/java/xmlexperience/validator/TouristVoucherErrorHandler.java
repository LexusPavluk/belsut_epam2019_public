package xmlexperience.validator;

import java.io.IOException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class TouristVoucherErrorHandler extends DefaultHandler {
    private Logger logger = Logger.getLogger(TouristVoucherErrorHandler.class.getName());

    public TouristVoucherErrorHandler() {
    }
    public TouristVoucherErrorHandler(String log) throws IOException {
        logger.addHandler(new FileHandler());
    }

    private String getLineAdress(SAXParseException e) {
        return e.getLineNumber() + " : " + e.getColumnNumber();
    }

    public void warning(SAXParseException e) {
        logger.warning(getLineAdress(e) + " - " + e.getMessage());
    }

    public void error(SAXParseException e) {
        logger.info(getLineAdress(e) + " - " + e.getMessage());
    }

    public void fatalError(SAXParseException e) {
        logger.severe(getLineAdress(e) + " - " + e.getMessage());
    }

}
