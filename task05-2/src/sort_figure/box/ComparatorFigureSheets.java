package sort_figure.box;

import sort_figure.figure.FigureSheet;
import java.util.Comparator;

public class ComparatorFigureSheets implements Comparator <FigureSheet> {
    private boolean ascend;

    public ComparatorFigureSheets(boolean ascend) {
        this.ascend = ascend;
    }

    private int ascendSign() {
        if(ascend) {
            return 1;
        } else return -1;
    }
    @Override
    public int compare(FigureSheet o1, FigureSheet o2) {
        return ascendSign() * o1.compareTo(o2);
    }

}
