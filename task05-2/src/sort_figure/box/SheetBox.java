package sort_figure.box;

import sort_figure.entities.IPaintable;
import sort_figure.entities.ISheet;
import sort_figure.exceptions.*;
import sort_figure.figure.Figure;
import sort_figure.figure.FigureSheet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.lang.String.valueOf;

/**
 * Container class for figures sheets.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 2.0
 */

public class SheetBox implements Serializable {
    private List<FigureSheet> container = new ArrayList<>();

    /**
     * Checking input data of correct values. Index of figure must be positive,
     * and less than total quantity of sheets.
     *
     * @param index - checking value.
     */
    private boolean correctIndexValue(int index) {
        return (index >= 0 &
                index < container.size());
    }

    /**
     * Adding one sheet to the box.
     *
     * @param figure - added sheet.
     */
    private boolean addFigure(FigureSheet figure) throws NullFigureException {
        if (figure == null) {
            throw new NullFigureException("The figure for adding is null");
        }
        if (container.contains(figure)) {
            throw new FigureAlreadyContainsInBoxException("Figure already contains in box");
        }
        container.add(figure);
        return true;
    }

    /**
     * Adding one and more different type sheets to the box.
     *
     * @param figures - added paper sheets.
     */
    public void addFigure(FigureSheet... figures) {
        for (int i = 0; i < figures.length; i++) {
            try {
                addFigure(figures[i]);
            } catch (NullFigureException e) {
                //System.err.println("The figure for adding is null");
            }
        }
    }


    /**
     * Getting one sheet of the box by number.
     *
     * @param num - geted sheet.
     */
    public FigureSheet getByNumber(int num) {
        if (correctIndexValue(num)) {
            return container.get(num);
        }
        throw new IllegalArgumentException("Incorrect index value");
    }

    /**
     * Remove one sheet out of the box by number.
     *
     * @param num - removed sheet.
     */
    public boolean removeByNumber(int num) {
        if (correctIndexValue(num) &&
                container.get(num) != null) {
            container.remove(num);
            return true;
        } else return false;
    }

    /**
     * Changing of one sheet to another in box by number.
     *
     * @param setFigure - changed sheet
     * @param num       - index location of another sheet.
     */
    public <T extends FigureSheet> FigureSheet replaceByNumber(T setFigure, int num) {
        FigureSheet gettingFigure = null;
        if (correctIndexValue(num)) {
            gettingFigure = getByNumber(num);
            container.set(num, setFigure);
        }
        return gettingFigure;
    }

    /**
     * Number of locations specified sheet in the box.
     *
     * @param figure - specified sheet.
     */
    public int indexPerSample(FigureSheet figure) {
        return container.indexOf(figure);
    }

    /**
     * Total quantity of sheets in the box.
     */
    public int getCountFigures() {
        return container.size();
    }

    /**
     * Total sheet's area in the box.
     */
    public double getTotalArea() {
        double totalArea = 0.0;
        for (FigureSheet elem : container) {
            totalArea += elem.getArea();
        }
        return totalArea;
    }

    /**
     * Total sheet's perimeter in the box.
     */
    public double totalPerimeter() {
        double totalPerimeter = 0.0;
        for (FigureSheet elem : container) {
            totalPerimeter += elem.getPerimeter();
        }
        return totalPerimeter;
    }

    /**
     * Getting all the sheets of specified class from the box.
     */
    public <T extends ISheet> List<FigureSheet> getFigures(Class<T> cl) {
        List<FigureSheet> returnedList = new ArrayList<>(container);
        returnedList.removeIf(elem -> !cl.isInstance(elem));
        return returnedList;
    }

    public <T extends ISheet> List<FigureSheet> getFigures(Class<T> cl, IPaintable.Palette color) {
        List<FigureSheet> returnedList = getFigures(cl);
        returnedList.removeIf(elem -> elem.getColor() != color);
        return returnedList;
    }

    public void sortBox(Comparator comparator) {
        container.sort(comparator);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (FigureSheet elem : container) {
            sb.append(elem + " " + String.format("%g ", elem.getArea()) + ",");
        }
        return "\n Box for the sheets {" +
                sb + '}';
    }
}
