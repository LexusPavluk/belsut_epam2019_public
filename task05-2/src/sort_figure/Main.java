package sort_figure;

import sort_figure.box.ComparatorFigureSheets;
import sort_figure.box.SheetBox;
import sort_figure.exceptions.NullFigureException;
import sort_figure.figure.*;
import sort_figure.figure.film.*;
import sort_figure.figure.paper.*;
import sort_figure.entities.IPaintable.*;

import java.util.Comparator;

/**
 * Point of entry to class and application - use to run demonstration and test the code.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 2.0
 */

public class Main {

    public static void main(String[] args) throws NullFigureException {

        SheetBox playBox = new SheetBox();  //create box girl's

        // create and add figures to box
        FilmCircle filmCircle13 = new FilmCircle(13.5);
        FilmTriangle filmTriangle15 = new FilmTriangle(15.3);
        FilmRectangle filmRectangle12_25 = new FilmRectangle(12.5, 25.0);
        playBox.addFigure(filmCircle13, filmTriangle15, filmRectangle12_25);

        FilmRectangle filmRectangle25_15 = new FilmRectangle(25.2, 15.7);
        FilmCircle filmCircle_7 = new FilmCircle(7.2, filmRectangle25_15);
        playBox.addFigure(filmCircle_7);

        PaperCircle paperCircle15 = new PaperCircle(15.0);
        paperCircle15.setColor(Palette.ROSE);
        paperCircle15.getColor();
        //paperCircle15.setColorSheet(Palette.BLACK); // try painting twice
        playBox.addFigure(paperCircle15);

        PaperTriangle paperTriangle5 = new PaperTriangle(5.1);
        paperTriangle5.setColor(Palette.RED);
        PaperTriangle paperTriangle3 = new PaperTriangle(3.3);
        paperTriangle3.setColor(Palette.BLUE);
        PaperCircle paperCircle6 = new PaperCircle(6.8);
        PaperCircle paperCircle = new PaperCircle(4.2);
        paperCircle.setColor(Palette.BLACK);
        playBox.addFigure(paperTriangle5, paperTriangle3, paperCircle6, paperCircle);
        //playBox.addFigure(paperCircle15); // trying to add same figure to Box

        PaperTriangle paperTriangle19 = new PaperTriangle(19.6);
        paperTriangle19.setColor(Palette.VIOLET);
        PaperRectangle paperRectangle6_4 = new PaperRectangle(6.2, 4.3, paperTriangle19);
        playBox.addFigure(paperRectangle6_4);

        System.out.println(playBox);
        // Get all figures specified form
        System.out.println(playBox.getFigures(Circle.class));
        System.out.println(playBox.getFigures(Triangle.class));
        System.out.println(playBox.getFigures(Rectangle.class));
        // Get all figures specified material
        System.out.println(playBox.getFigures(IFilm.class));
        System.out.println(playBox.getFigures(IPaper.class));
        // Get all figures specified form and color
        System.out.println(playBox.getFigures(Circle.class, Palette.ROSE));
        System.out.println(playBox.getFigures(Triangle.class, Palette.BLUE));
        System.out.println(playBox.getFigures(Rectangle.class, Palette.VIOLET));
        // Get all figures specified color
        System.out.println(playBox.getFigures(IPaper.class, Palette.WHITE));
        // sort SheetBox by color ( inner IPaintable.ColorComparator )
        playBox.sortBox(new ColorComparator(true));
        System.out.println(playBox);
        playBox.sortBox(new ColorComparator(false));
        System.out.println(playBox);
        // sort SheetBox by area ( class ComparatorFigureSheets )
        playBox.sortBox(new ComparatorFigureSheets(true));
        System.out.println(playBox);
        playBox.sortBox(new ComparatorFigureSheets(false));
        System.out.println(playBox);
        // sort SheetBox by Form (anonymous Comparator)
        playBox.sortBox(new Comparator<FigureSheet>() {

            @Override
            public int compare(FigureSheet o1, FigureSheet o2) {
                return o1.getClass().getSimpleName().compareTo(o2.getClass().getSimpleName());
            }
        });
        System.out.println(playBox);


/*
      // work with box
        playBox.getCountFigures();
        playBox.getByNumber(5);
        playBox.removeByNumber(5);
        IFilm filmRectangle7_3 = new FilmRectangle(7.3, 3.2);
        playBox.replaceByNumber((FigureSheet) filmRectangle7_3, 6);
        playBox.indexPerSample(paperTriangle5);
        playBox.getTotalArea();
        playBox.totalPerimeter();
        System.out.println(playBox);
*/

/*        String fileName = "resources\\box.data";

        Serializator serializator = new Serializator();
        boolean bool = false;
        try {
            bool = serializator.serialization(playBox, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(bool);

        //fileName = "resources\\box1.data";
        SheetBox otherGirLBox = null;
        try {
            otherGirLBox = serializator.deserialization(fileName);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }
        System.out.println(otherGirLBox);
*/
    }
}