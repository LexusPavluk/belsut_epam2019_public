package sort_figure.entities;

/**
 * Marker interface of Sheet - cutout material.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */


public interface ISheet extends IPaintable {
}
