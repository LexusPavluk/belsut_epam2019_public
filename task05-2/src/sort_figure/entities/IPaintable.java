package sort_figure.entities;

import sort_figure.exceptions.FigureNotMayBePaintedException;
import java.io.Serializable;
import java.util.Comparator;

/**
 * The marker interface describes "Painting" property and "Color identifications"
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 2.0
 */

public interface IPaintable {

    /**
     * Determining the color of material.
     *
     * @return color of object.
     * @see Palette
     */
    Palette getColor();

    /**
     *
     * @param newColor - set color of material for <tt>Palette</tt>.
     * @see Palette
     */
    void setColor(Palette newColor);


    enum Palette {
        TRANSPARENT,
        WHITE,
        RED,
        ROSE,
        ORANGE,
        YELLOW,
        GREEN,
        BLUE,
        VIOLET,
        BLACK
    }


    /**
     * Inner class <tt>Color</tt> for storing value of paper color.
     */
    class Painted implements Serializable {

        /**
         * The {@code Colors} value of
         * <i>color</i> .
         */
        private Palette color;

        /**
         * Default constructor of class <tt>Color</tt> sets the color value WHITE.
         */
        public Painted() {
            this.color = Palette.WHITE;
        }


        /**
         * Painting object.
         *
         * @param newColor - set color of object for <tt>Colors</tt>.
         * @see Palette
         */
        public boolean setColor(Palette newColor) {
            if (color != Palette.WHITE) {
                throw new FigureNotMayBePaintedException("This figure is already painted");
            } else {
                color = newColor;
                return true;
            }
        }

        /**
         * Color identification.
         *
         * @return color of object.
         * @see Palette
         */
        public Palette getColor() {
            return color;
        }

    }

    class ColorComparator implements Comparator<IPaintable> {
        private boolean ascend;

        public ColorComparator(boolean ascend) {
            this.ascend = ascend;
        }

        private int ascendSign() {
            if(ascend) {
                return 1;
            } else return -1;
        }

        @Override
        public int compare(IPaintable o1, IPaintable o2) {
            return ascendSign()*o1.getColor().compareTo(o2.getColor());
        }
    }
}
