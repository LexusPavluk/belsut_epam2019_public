package sort_figure.exceptions;

public class FigureNotMayBePaintedException extends RuntimeException {
    public FigureNotMayBePaintedException() {
    }

    public FigureNotMayBePaintedException(String message) {
        super(message);
    }
}
