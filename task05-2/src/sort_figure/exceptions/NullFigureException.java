package sort_figure.exceptions;

public class NullFigureException extends Exception {
    public NullFigureException() {
    }

    public NullFigureException(String s) {
            super(s);
    }
}
