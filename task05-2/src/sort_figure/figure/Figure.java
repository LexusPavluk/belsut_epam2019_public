package sort_figure.figure;

import java.io.Serializable;
import java.util.Objects;

/**
 * Abstract class of Figure - provides get area and perimeter from Figure, also get defining size of Figure.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.2
 */
public abstract class Figure implements Serializable, Comparable<Figure> {

    /**
     * Calculate area of figure.
     *
     * @return area of figure.
     */
    public abstract double getArea();

    /**
     * Calculate perimeter of Figure.
     *
     * @return perimeter of Figure.
     */
    public abstract double getPerimeter();

    /**
     * Calculate defining size of Figure. On the basis this size, the ability to cut a Figure from another is evaluated.
     * So for example in triangle defining size is radius of inscribed circle, for rectangle - half of small side,
     * for circle - radius.
     *
     * @return defining size.
     */

    public abstract double getDefiningSize();

    @Override
    public String toString() {
        return "\n" + this.getClass().getSimpleName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClass().getSimpleName());
    }

    @Override
    public int compareTo(Figure o) {
        return Double.compare(this.getArea(), o.getArea());
    }

}
