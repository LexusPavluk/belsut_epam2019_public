package sort_figure.figure;

import java.util.Objects;

import static java.lang.Math.*;

/**
 * Abstract class of Rectangle - implements methods of abstract class <tt> Figure </tt>, from which extended.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public abstract class Rectangle extends FigureSheet {

    /**
     * The {@code double} value of first
     * <i>SIDE1</i> from Rectangle.
     */
    protected final double side1;

    /**
     * The {@code double} value of second
     * <i>SIDE2</i> from Rectangle.
     */
    protected final double side2;

    /**
     * Constructor of class Rectangle with specified sides.
     *
     * @param side1 first side for making Rectangle.
     * @param side2 second side for making Rectangle.
     */
    public Rectangle(double side1, double side2) {
        ValidationUtils.verificationPositivityNumber(side1);
        ValidationUtils.verificationPositivityNumber(side2);
        this.side1 = side1;
        this.side2 = side2;
    }

    /**
     * Constructor of class Rectangle with specified sides, and the <tt>Figure</tt> from which it is cut.
     *
     * @param side1  first side for making Rectangle.
     * @param side2  second side for making Rectangle.
     * @param figure Figure which cut.
     */
    public Rectangle(double side1, double side2, Figure figure) {
        ValidationUtils.verificationPositivityNumber(side1);
        ValidationUtils.verificationPositivityNumber(side2);
        ValidationUtils.checkPossibilityMakeFigure(figure, side1, side2);
        this.side1 = side1;
        this.side2 = side2;
    }

    /**
     * Getter for first side of Rectangle.
     *
     * @return side1 first side for making Rectangle.
     */
    public double getSide1() {
        return side1;
    }

    /**
     * Getter for second side of Rectangle.
     *
     * @return side2 second side for making Rectangle.
     */
    public double getSide2() {
        return side2;
    }

    @Override
    public double getArea() {
        return side1 * side2;
    }

    @Override
    public double getPerimeter() {
        return (side1 + side2) * 2;
    }

    @Override
    public double getDefiningSize() {
        return min(side1, side2) / 2;
    }

    @Override
    public String toString() {
        return "\n" + this.getClass().getSimpleName() + " " + side1 + "*" + side2;
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            Rectangle other = (Rectangle) obj;
            return (Double.compare(side1, other.getSide1()) == 0 &&
                    Double.compare(side2, other.getSide2()) == 0);
        } else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), side1, side2);
    }

}
