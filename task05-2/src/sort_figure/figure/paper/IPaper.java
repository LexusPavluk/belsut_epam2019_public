package sort_figure.figure.paper;

import sort_figure.entities.IPaintable;
import sort_figure.entities.ISheet;

/**
 * Marker interface of Paper - description of the Painted Sheet
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.1
 */

public interface IPaper extends ISheet {

}
