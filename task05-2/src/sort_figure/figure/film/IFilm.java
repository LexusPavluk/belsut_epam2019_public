package sort_figure.figure.film;

import sort_figure.entities.IPaintable;
import sort_figure.entities.ISheet;

/**
 * Marker interface of Film.
 * The Film is transparent, so painting methods are no available.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */


public interface IFilm extends ISheet {

    @Override
    default Palette getColor() {
        return Palette.TRANSPARENT;
    }

    @Override
    default void setColor(Palette newColor) {
    }
}
