package sort_figure.figure.film;

import sort_figure.figure.*;

/**
 * FilmTriangle class - extend of abstract class <tt>Triangle</tt>.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public class FilmTriangle extends Triangle implements IFilm {

    /**
     * Constructor of class FilmTriangle with specified side.
     *
     * @param side - side of isosceles Triangle.
     */
    public FilmTriangle(double side) {
        super(side);
    }

    /**
     * Constructor of class FilmTriangle with specified side, and the <tt>Figure</tt> from which it is cut.
     *
     * @param side   - side of isosceles FilmTriangle.
     * @param figure - Figure which cut.
     */
    public FilmTriangle(double side, IFilm figure) {
        super(side, (Figure) figure);
    }

}
