package sort_figure;

import sort_figure.box.SheetBox;
import java.io.*;

public class Serializator {
    public boolean serialization(SheetBox box, String fileName) throws Exception {
        boolean flag = false;
        File file = new File(fileName);
        ObjectOutputStream outputStream = null;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            outputStream = new ObjectOutputStream(fileOutputStream);
            outputStream.writeObject(box);
            flag = true;
        } catch (FileNotFoundException e) {
            System.err.println("File not may be created " + e);
        } catch (NotSerializableException e) {
            System.err.println("Class does not support serialisation " + e);
        } catch (IOException e) {
            throw new Exception("Input-output error " + e);
//            System.err.println("Input-output error " + e);
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Stream close error " + e);
            }
        } return flag;
    }

    public SheetBox deserialization(String fileName) throws InvalidObjectException {
        File readingFile = new File(fileName);
        ObjectInputStream inputStream = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(readingFile);
            inputStream = new ObjectInputStream(fileInputStream);
            return (SheetBox) inputStream.readObject();
        } catch (ClassNotFoundException e) {
            System.err.println("Class does not exist " + e);
        } catch (FileNotFoundException e) {
            System.err.println("File for serialisation does not found " + e);
        } catch (InvalidClassException e) {
            System.err.println("Class version mismatch " + e);
        } catch (IOException e) {
            System.err.println("Input-output error " + e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Stream close error " + e);
            }
        } throw new InvalidObjectException("The object is not restored");
    }
}
