package lexuspavluk.beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Task implements Identifiable {
    private Integer id;
    private String note;
    private Date date;
    private Status status;
    private String filename;
    private Integer ts_user;
    private static final DateFormat DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd");

    public Task() {}

    public Task(Integer id, String note, String date, String status,
                  String filename, Integer ts_user) {
        this.id = id;
        this.note = note;
        setDate(date);
        setStatus(status);
        this.filename = filename;
        this.ts_user = ts_user;
    }

    @Override
    public Integer getId() {
        return id;
    }
    protected void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }

    public String getDate() {
        return DATE_FORMAT.format(date);
    }
    public void setDate(String date) {
        try {
            this.date = DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getStatus() {
        return status.toString();
    }
    public void setStatus(String status) {
        this.status = Status.valueOf(status.toUpperCase());
    }

    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getTs_user() {
        return ts_user;
    }
    public void setTs_user(Integer ts_user) {
        this.ts_user = ts_user;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", note='" + note + '\'' +
                ", date=" + date +
                ", status=" + status +
                ", filename='" + filename + '\'' +
                ", ts_user=" + ts_user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        Task task = (Task) o;
        return getTs_user().equals(task.getTs_user()) &&
                getId().equals(task.getId()) &&
                getNote().equals(task.getNote()) &&
                getDate().equals(task.getDate()) &&
                getStatus().equals(task.getStatus()) &&
                getFilename().equals(task.getFilename());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNote(), getDate(), getStatus(), getFilename(), getTs_user());
    }

    public enum Status {
        TODO, DONE, RECYCLED;
    }
}
