package lexuspavluk.beans;

import java.util.ArrayList;
import java.util.List;

public class Tasks {
    private String tableType;
    private List<Task> data = new ArrayList<>();

    public Tasks() {
    }

    public Tasks(String tableType, List<Task> data) {
        this.tableType = tableType;
        this.data = data;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public List<Task> getData() {
        return data;
    }

    public void setData(List<Task> data) {
        this.data = data;
    }
}
