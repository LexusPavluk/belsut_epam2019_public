package lexuspavluk.beans;

import java.io.Serializable;

public interface Identifiable extends Serializable, Cloneable {

    public Integer getId();

}
