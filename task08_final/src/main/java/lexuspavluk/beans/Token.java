package lexuspavluk.beans;

public class Token implements Identifiable {
    private String token;
    private Integer user_id;



    public Token(String token, int user_id) {
        this.token = token;
        this.user_id = user_id;
    }


    @Override
    public Integer getId() {
        return user_id;
    }
}
