package lexuspavluk.beans;

import java.util.Objects;

public class User implements Identifiable, Keyable {
    private Integer id = null;
    private String login;
    private String password;

    public User(){
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User(Integer user_id, String login, String password) {
        this.id = user_id;
        this.login = login;
        this.password = password;

    }
	
	public User(Integer user_id, String login) {
	this.id = user_id;
	this.login = login;
    }



    @Override
    public Integer getId() {
        return id;
    }
    protected void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId().equals(user.getId()) &&
                getLogin().equals(user.getLogin()) &&
                getPassword().equals(user.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLogin(), getPassword());
    }
}
