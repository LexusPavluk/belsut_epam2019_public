package lexuspavluk.dao.sql;

import lexuspavluk.beans.Identifiable;
import lexuspavluk.dao.DAOFactory;
import lexuspavluk.dao.GenericDAO;
import lexuspavluk.ConnectionPool;

import java.sql.Connection;

public class MySQLDAOFactory implements DAOFactory {
    private ConnectionPool pool;
    private static MySQLDAOFactory instance = new MySQLDAOFactory();

    private MySQLDAOFactory() {
        pool = ConnectionPool.getInstance();
        }

    public static MySQLDAOFactory getInstance() {
        return instance;
    }

    @Override
    public <T extends Identifiable> GenericDAO getDAO(T dtoClass) {
        String type = dtoClass.getClass().getSimpleName();
        switch (type) {
            case "User":
                return new MySQLUsersDao(getConnection());
            case "Task":
                return new MySQLTaskDao(getConnection());
            default:
                throw new IllegalArgumentException("Invalid dtoType: " + dtoClass);
        }
    }

    @Override
    public Connection getConnection() {
        return pool.getConnection();
    }

}
