package lexuspavluk.dao.sql;

import lexuspavluk.beans.User;
import lexuspavluk.dao.sql.exceptions.MySQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static lexuspavluk.AppConst.*;

public class MySQLUsersDao extends MySQLAbstractDAO<User> {

    {
        createQuery = "INSERT INTO mydb.users (" + COL_USER_LOGIN + ", " +
                COL_USER_PASS + ") VALUES (?, ?);";
        selectAllQuery = "SELECT * FROM mydb.users";
        updateQuery = ";";
        deleteQuery = "DELETE FROM mydb.users WHERE " + COL_USER_ID + "= ?;";
    }

    MySQLUsersDao(Connection connection) {
        super(connection);
    }

    @Override
    public List<User> parseResultSet(ResultSet rs) {
        List<User> result = new LinkedList<>();
        try {
            while (rs.next()) {
                User user = new User(rs.getInt(COL_USER_ID),
                        rs.getString(COL_USER_LOGIN),
                        rs.getString(COL_USER_PASS));
                result.add(user);
            }
        } catch (SQLException e) {
            throw new MySQLException("Result set parsed is wrong");
        }
        return result;
    }

    @Override
    public void prepareStatementForCreate(PreparedStatement ps, User object) {
        try {
            ps.setString(1, object.getLogin());
            ps.setString(2, object.getPassword());
        } catch (SQLException e) {
            throw new MySQLException("Error of prepared statement to crate record");
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement ps, User object) {
        throw new UnsupportedOperationException("Update is impossible");
    }

    @Override
    public int getIdByDTO(User wantedUser) {
        List<User> list;
        try (PreparedStatement prepStatem = connection.prepareStatement(
                selectAllQuery + " WHERE "
                        + COL_USER_LOGIN + " = ? and " + COL_USER_PASS + " = ?")) {
            prepStatem.setString(1, wantedUser.getLogin());
            prepStatem.setString(2, wantedUser.getPassword());
            ResultSet resultSet = prepStatem.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new MySQLException("SQL data base error!!");
        }
        if (list == null || list.size() == 0) {
            return -1;
        } else {
            return list.get(0).getId();
        }
    }

}
