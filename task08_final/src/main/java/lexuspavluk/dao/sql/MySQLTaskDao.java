package lexuspavluk.dao.sql;

import lexuspavluk.TablesGetEnum;
import lexuspavluk.beans.Task;
import lexuspavluk.dao.sql.exceptions.MySQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static lexuspavluk.AppConst.*;

public class MySQLTaskDao extends MySQLAbstractDAO<Task> {
    {
        createQuery = "INSERT INTO mydb.tasks (" + COL_TASKS_NOTE + ", " +
                COL_TASKS_DATE + ", " + COL_TASKS_USER_KEY + ") \n VALUES (?, ?, ?);";
        selectAllQuery = "SELECT * FROM mydb.tasks";
        updateQuery = "UPDATE mydb.tasks SET " + COL_TASKS_NOTE + "=?, " +
                COL_TASKS_DATE + "=?, " + COL_TASKS_STATUS + "=?, " +
                COL_TASKS_FILENAME + "=? WHERE " + COL_TASKS_ID + " = ?;";
        deleteQuery = "DELETE FROM mydb.tasks WHERE " + COL_TASKS_ID + "= ?;";
    }

    MySQLTaskDao(Connection connection) {
        super(connection);
    }

    @Override
    public List<Task> parseResultSet(ResultSet rs) {
        List<Task> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Task task = new Task(rs.getInt(COL_TASKS_ID),
                        rs.getString(COL_TASKS_NOTE),
                        rs.getString(COL_TASKS_DATE),
                        rs.getString(COL_TASKS_STATUS),
                        rs.getString(COL_TASKS_FILENAME),
                        rs.getInt(COL_TASKS_USER_KEY));
                result.add(task);
            }
        } catch (SQLException e) {
            throw new MySQLException("Result set parsing is wrong");
        }
        return result;
    }

    @Override
    public void prepareStatementForCreate(PreparedStatement ps, Task object) {
        try {
            ps.setString(1, object.getNote());
            ps.setString(2, object.getDate());
            ps.setInt(3, object.getTs_user());
        } catch (SQLException e) {
            throw new MySQLException("Error of prepared statement to crate record");
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement ps, Task object) {
        try {
            ps.setString(1, object.getNote());
            ps.setString(2, object.getDate());
            ps.setString(3, object.getStatus());
            ps.setString(4, object.getFilename());
            ps.setInt(5, object.getId());
        } catch (SQLException e) {
            throw new MySQLException("Error of prepared statement to update record");
        }
    }

    @Override
    public int getIdByDTO(Task dtoObject) {
        throw new UnsupportedOperationException("Operation is impossible");
    }

    public List<Task> getAllByUserIdAndTableType(Integer user_id, TablesGetEnum tableType) {
        String postfix = " WHERE " + COL_TASKS_USER_KEY + "=" + user_id + tableType.getQuerySelectPostfix();
        return getAll(selectAllQuery + postfix);
    }

    public List<Task> getAllByFileName(String fileName) {
        String postfix = " WHERE " + COL_TASKS_FILENAME + "= '" + fileName +"' " ;
        return getAll(selectAllQuery + postfix);
    }


}