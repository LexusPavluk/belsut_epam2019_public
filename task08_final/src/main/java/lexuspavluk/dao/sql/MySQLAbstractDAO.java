package lexuspavluk.dao.sql;

import lexuspavluk.beans.Identifiable;
import lexuspavluk.dao.GenericDAO;
import lexuspavluk.dao.sql.exceptions.MySQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public abstract class MySQLAbstractDAO<T extends Identifiable>
        implements GenericDAO<T> {

    Connection connection;

    MySQLAbstractDAO(Connection connection) {
        this.connection = connection;
    }

    String createQuery;
    /**
     * Устанавливает аргументы create запроса в соответствии со значением полей объекта object.
     */
    public abstract void prepareStatementForCreate(PreparedStatement ps, T object);

    @Override
    public T createRecord(T entity) {
        T recordSQL;
        try (PreparedStatement prepStatem = connection.prepareStatement(createQuery)) {
            prepareStatementForCreate(prepStatem, entity);
            int count = prepStatem.executeUpdate();
            if (count != 1) {
                throw new MySQLException("Storing in DB, modify more than 1 record: " + count);
            }
        } catch (SQLException e) {
            throw new MySQLException("Failed to create record!");
        }
        try (PreparedStatement prepStatem =
                     connection.prepareStatement( selectAllQuery + " ORDER BY id DESC LIMIT 1;")) {
            ResultSet resultSet = prepStatem.executeQuery();
            List<T> list = parseResultSet(resultSet);
            if(list == null || list.size() != 1) {
                throw new MySQLException("Failed to return created record!");
            } else {
                recordSQL = list.iterator().next();
            }
        } catch (SQLException e) {
            throw new MySQLException("Failed to return created record!");
        }
        return recordSQL;
    }

    String selectAllQuery;
    /**
     * Разбирает ResultSet и возвращает список объектов соответствующих содержимому ResultSet.
     */
    public abstract List<T> parseResultSet(ResultSet rs);

    @Override
    public T getByPK(Integer pKey) {
        List<T> list = null;
        try (PreparedStatement prepStatem = connection.prepareStatement(selectAllQuery + " WHERE id = ?;")) {
            prepStatem.setInt(1, pKey);
            ResultSet resultSet = prepStatem.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (list == null || list.size() == 0) {
            throw new MySQLException("Row by prymary key " + pKey + " is not found.");
        }
        if (list.size() > 1) {
            throw new MySQLException("Found more than 1 record by " + pKey);
        }
        return list.get(0);
    }

    @Override
    public List<T> getAll(String query) {
        List<T> list;
        try (PreparedStatement prepStatem = connection.prepareStatement(query + ";")) {
            ResultSet resultSet = prepStatem.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new MySQLException(e);
        }
        return list;
    }

    String updateQuery;
    /**
     * Устанавливает аргументы update запроса в соответствии со значением полей объекта object.
     */
    public abstract void prepareStatementForUpdate(PreparedStatement ps, T object) throws SQLException;

    @Override
    public boolean update(T entity) {
        boolean isUpdated;
        try (PreparedStatement prepStatem = connection.prepareStatement(updateQuery)) {
            prepareStatementForUpdate(prepStatem, entity);
            int count = prepStatem.executeUpdate();
            isUpdated = (count == 1);
        } catch (Exception e) {
            throw new MySQLException("Updating modified more than 1 record!");
        }
        return isUpdated;
    }

    String deleteQuery;
    @Override
    public boolean delete(Integer pKey) {
        boolean isDeleted;
        try (PreparedStatement prepStatem = connection.prepareStatement(deleteQuery)) {
            prepStatem.setObject(1, pKey);
            int count = prepStatem.executeUpdate();
            isDeleted = (count == 1);
        } catch (Exception e) {
            throw new MySQLException("Deleting modified more than 1 record!");
        }
        return isDeleted;
    }

    @Override
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
