package lexuspavluk.dao;

import lexuspavluk.beans.Identifiable;

import java.sql.Connection;
import java.sql.SQLException;

public interface DAOFactory {

    Connection getConnection() throws SQLException;

    <T extends Identifiable> GenericDAO getDAO(T dtoClass);
    //void closeConnection();

/*
    <T extends AbstractDAO<Identifiable>> T getDAO(Class<Identifiable> dtoClass,
                          Connection connect);
*/
}
