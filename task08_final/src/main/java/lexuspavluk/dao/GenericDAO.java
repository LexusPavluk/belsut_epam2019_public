package lexuspavluk.dao;

import lexuspavluk.beans.Identifiable;

import java.sql.SQLException;
import java.util.List;

public interface GenericDAO<T extends Identifiable> {

    T createRecord(T entity);

    T getByPK(Integer pKey);

    int getIdByDTO(T dtoObject);

    boolean update(T entity);

    boolean delete(Integer id);

    List<T> getAll(String query);

    void closeConnection();

}
