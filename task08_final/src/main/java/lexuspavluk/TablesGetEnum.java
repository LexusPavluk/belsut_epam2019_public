package lexuspavluk;

import static lexuspavluk.AppConst.COL_TASKS_DATE;
import static lexuspavluk.AppConst.COL_TASKS_STATUS;

public enum TablesGetEnum {
    TODAY {
        {this.querySelectPostfix = " AND " + COL_TASKS_DATE + " <= CURDATE() AND "
                + COL_TASKS_STATUS + " ='TODO'";}
    },
    TOMORROW {
        {this.querySelectPostfix = " AND " + COL_TASKS_DATE + " = DATE_ADD(CURDATE(), INTERVAL 1 DAY) AND "
                + COL_TASKS_STATUS + " ='TODO'";}
    },
    SOMEDAY {
        {this.querySelectPostfix = " AND " + COL_TASKS_DATE + " > DATE_ADD(CURDATE(), INTERVAL 1 DAY) AND "
                + COL_TASKS_STATUS + " ='TODO'";}
    },
    DONE {
        {this.querySelectPostfix = " AND " + COL_TASKS_STATUS + " = 'DONE'";}
    },
    RECYCLED{
        {this.querySelectPostfix = " AND " + COL_TASKS_STATUS + " = 'RECYCLED'";}
    };
    protected String querySelectPostfix;

    public String getQuerySelectPostfix() {
        return querySelectPostfix;
    }
}
