package lexuspavluk;

import static lexuspavluk.AppConst.COL_TASKS_DATE;
import static lexuspavluk.AppConst.COL_TASKS_STATUS;

public enum UpdateTypesEnum {
    FILE_PUSH {
        {this.querySelectPostfix = "";}
    },
    FILE_GET {
        {this.querySelectPostfix = "";}
    },
    LATER {;
        {this.querySelectPostfix = "";}
    },
    DONE {
        {this.querySelectPostfix = "";}
    },
    RECYCLE{
        {this.querySelectPostfix = "";}
    },
	RESTORE{
        {this.querySelectPostfix = "";}
    },
    DELETE{
        {this.querySelectPostfix = "";}
    },
    DELETE_ALL{
        {this.querySelectPostfix = "";}
    };
    protected String querySelectPostfix;

    public String getQuerySelectPostfix() {
        return querySelectPostfix;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
