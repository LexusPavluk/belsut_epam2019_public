package lexuspavluk;

import lexuspavluk.service.commands.FileUploadComm;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.server.ExportException;

@MultipartConfig
@WebServlet(name = "loader", urlPatterns = {"/upload"})
public class ControllerUpload extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws IOException {
        String fileName = request.getParameter("attachment");
        File file = new File( AppConst.STORAGE_FOLDER_PATH + File.separator + fileName);
        ServletOutputStream outputStream = null;
        BufferedInputStream inputStream = null;
        try {
            outputStream = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            response.setContentLength((int) file.length());
            FileInputStream fileInputStream = new FileInputStream(file);
            inputStream = new BufferedInputStream(fileInputStream);
            int readBytes;
            while ((readBytes = inputStream.read()) != -1)
                outputStream.write(readBytes);
        }catch (ExportException e){
            e.printStackTrace();
        }finally {
            outputStream.flush();
            outputStream.close();
            inputStream.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {
        Part idPart = request.getPart(AppConst.PARAM_TASK_ID);
        Integer task_id = Integer.parseInt(readString(idPart));

        Part filePart = request.getPart(AppConst.PARAM_ATTACHMENT);
        String fileName = getFileName(filePart);

        if (filePart != null && filePart.getSize() > 0
                && filePart.getInputStream() != null && fileName != null) {

            final Path outputFile = Paths.get(AppConst.STORAGE_FOLDER_PATH, fileName);

            try (final ReadableByteChannel input =
                         Channels.newChannel(filePart.getInputStream());
                 final WritableByteChannel output =
                         Channels.newChannel(new FileOutputStream(outputFile.toFile()));) {
                pipe(input, output);
            }

            FileUploadComm updateComm = new FileUploadComm(task_id, fileName);
            String agree = updateComm.execute(request);
        }
        request.setAttribute("message", "Upload has been done successfully!");
        getServletContext().getRequestDispatcher("/message.jsp").forward(
                request, response);

    }



    private static String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim()
                        .replace("\"", "");
            }
        }
        return null;
    }

    private static void pipe(ReadableByteChannel in, WritableByteChannel out)
            throws IOException {
        final ByteBuffer buffer = ByteBuffer.allocate(1024);
        while (in.read(buffer) >= 0 || buffer.position() > 0) {
            buffer.flip();
            out.write(buffer);
            buffer.compact();
        }
    }

    private static String readString(Part part) throws IOException
    {
        try (InputStreamReader reader = new InputStreamReader(part.getInputStream())) {
            StringBuilder stringBuilder = new StringBuilder();
            char[] buffer = new char[1024];
            for (int c = 0; (c = reader.read(buffer)) != -1; )
                stringBuilder.append(buffer, 0, c);
            return stringBuilder.toString();
        }
    }

}