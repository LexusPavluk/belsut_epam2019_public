package lexuspavluk.service;

import lexuspavluk.beans.User;
import lexuspavluk.dao.DAOFactory;
import lexuspavluk.dao.GenericDAO;
import lexuspavluk.dao.sql.MySQLDAOFactory;
import lexuspavluk.dao.sql.MySQLUsersDao;

public class LoginLogic {
    private static DAOFactory factory = MySQLDAOFactory.getInstance();

    public static int checkLogin(User user) {
        GenericDAO<User> userDAO = (MySQLUsersDao) factory.getDAO(user);
        int id = userDAO.getIdByDTO(user);
		userDAO.closeConnection();
		return id;
    }
}
