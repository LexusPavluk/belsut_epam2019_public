package lexuspavluk.service;

import lexuspavluk.AppConst;
import lexuspavluk.dao.DAOFactory;
import lexuspavluk.dao.sql.MySQLDAOFactory;

import javax.servlet.http.HttpServletRequest;

public interface ActionCommand {
    DAOFactory factory = MySQLDAOFactory.getInstance();

    String execute(HttpServletRequest request);
}
