package lexuspavluk.service;

import lexuspavluk.service.commands.*;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {
    synchronized public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action +
                    Messages.WRONG_ACTION);
        }
        return current;
    }

    public enum CommandEnum {
        REGISTRATION {
            {
                this.command = new RegisterComm();
            }
        },
        LOGIN {
            {
                this.command = new LoginComm();
            }
        },
        LOGOUT {
            {
                this.command = new LogoutComm();
            }
        },
        NEW_TASK {
            {
                this.command = new TasksCreateComm();
            }
        },
        UPDATE_TASK {
            {
                this.command = new TasksUpdateComm();
            }
        },
        GET_TASKS {
            {
                this.command = new TasksGetComm();
            }
        },
        DELETE_TASK {
            {
                this.command = new TasksDeleteComm();
            }
        },
        DELETE_ALL_TASK {
            {
                this.command = new TasksDeleteAllComm();
            }
        };

        ActionCommand command;

        public ActionCommand getCurrentCommand() {
            return command;
        }
    }
}
