package lexuspavluk.service;

public class Messages {
    public static final String WRONG_ACTION = ": command not found or wrong!";
    public static final String NULL_PAGE = "Page not found. Business logic error.";
    public static final String LOGIN_ERROR = "Incorrect login or password.";
    public static final String REGISTRATION_ERROR = "Internal registration error.";
}
