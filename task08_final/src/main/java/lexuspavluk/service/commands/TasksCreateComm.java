package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.beans.Task;
import lexuspavluk.beans.User;
import lexuspavluk.dao.GenericDAO;
import lexuspavluk.dao.sql.MySQLTaskDao;
import lexuspavluk.service.ActionCommand;

import javax.servlet.http.HttpServletRequest;

public class TasksCreateComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute(AppConst.PARAM_USER);
        String ts_note = request.getParameter("note");
        String ts_date = request.getParameter("date");
        Task createdTask = new Task(null, ts_note, ts_date, "TODO", null, user.getId());
        GenericDAO<Task> taskDAO = (MySQLTaskDao) factory.getDAO(createdTask);
        createdTask = taskDAO.createRecord(createdTask);  // todo - make check
        taskDAO.closeConnection();
        return new TasksGetComm().execute(request);
    }
}
