package lexuspavluk.service.commands;

import lexuspavluk.beans.Task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public enum UpdateTypesEnum {
    LATER, DONE, RECYCLE, RESTORE;

    public Task getTaskToUpdate(Task task){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        switch (this) {
            case DONE:
                task.setStatus(Task.Status.DONE.toString());
                return task;
            case RECYCLE:
                task.setStatus(Task.Status.RECYCLED.toString());
                return task;
            case RESTORE:
                task.setStatus(Task.Status.TODO.toString());
                String today = df.format(new Date());
                task.setDate(today);
                return task;
            case LATER:
                try {
                    Date initDate = df.parse(task.getDate());
                    initDate.setTime(initDate.getTime() + (long)(24 * 60 * 61 * 1000));
                    task.setDate(df.format(initDate));
                    return task;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
        }
        return task;
    }

}
