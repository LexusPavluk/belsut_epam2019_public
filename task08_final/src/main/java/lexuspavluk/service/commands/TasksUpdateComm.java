package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.beans.Task;
import lexuspavluk.beans.User;
import lexuspavluk.dao.GenericDAO;
import lexuspavluk.dao.sql.MySQLTaskDao;
import lexuspavluk.service.ActionCommand;

import javax.servlet.http.HttpServletRequest;

public class TasksUpdateComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        Task updatedTask = getTaskToUpdate(request);
        GenericDAO<Task> taskDAO = (MySQLTaskDao) factory.getDAO(updatedTask);
        boolean isTaskUpdate = taskDAO.update(updatedTask);   // todo !! boolean !!
        taskDAO.closeConnection();
        return new TasksGetComm().execute(request);
    }

    private Task getTask (HttpServletRequest request) {
        Integer ts_id = Integer.parseInt(request.getParameter("task_id"));
        String ts_note = request.getParameter("task_note");
        String ts_date = request.getParameter("task_date");
        String ts_status = request.getParameter("task_status");
        String ts_file =request.getParameter("task_file");
        User user = (User) request.getSession().getAttribute(AppConst.PARAM_USER);
        return new Task(ts_id, ts_note, ts_date, ts_status, ts_file, user.getId());
    }

    private Task getTaskToUpdate (HttpServletRequest request) {
        Task initialTask = getTask(request);
        String updateType = request.getParameter("update");
        UpdateTypesEnum typeUpdate = UpdateTypesEnum.valueOf(updateType.toUpperCase());
        return typeUpdate.getTaskToUpdate(initialTask);
    }

}