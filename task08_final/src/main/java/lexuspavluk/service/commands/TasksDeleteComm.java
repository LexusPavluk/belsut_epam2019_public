package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.beans.Task;
import lexuspavluk.dao.sql.MySQLTaskDao;
import lexuspavluk.service.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

public class TasksDeleteComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        Integer ts_id = Integer.parseInt(request.getParameter("task_id"));
        taskDelete(ts_id);
        return new TasksGetComm().execute(request);
    }

    private void fileDelete (String fileName) {
        File deletedFile = new File(AppConst.STORAGE_FOLDER_PATH + File.separator + fileName);
        if (deletedFile.exists()) {
            deletedFile.delete();
        }
    }

    void taskDelete (Integer tsId) {
        MySQLTaskDao taskDAO = (MySQLTaskDao) factory.getDAO(new Task());
        Task deletedTask = taskDAO.getByPK(tsId);
        String fName = deletedTask.getFilename();
        List<Task> taskList = taskDAO.getAllByFileName(fName);
        if (taskList.size() == 1) {
            fileDelete(fName);
        }
        boolean isTaskDelete = taskDAO.delete(tsId);  //todo - make check
        taskDAO.closeConnection();
    }
}