package lexuspavluk.service.commands;

import lexuspavluk.beans.Task;
import lexuspavluk.beans.Tasks;
import lexuspavluk.service.ActionCommand;
import lexuspavluk.service.TablesLogic;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class TasksDeleteAllComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        TablesLogic tl = new TablesLogic(request);
        Tasks tasks = tl.getTasksTable();
        List<Task> taskList = tasks.getData();

        TasksDeleteComm td = new TasksDeleteComm();
        for (Task task: taskList) {
            Integer ts_id = task.getId();
            td.taskDelete(ts_id);
        }

        return new TasksGetComm().execute(tl.getRequest());
    }
}
