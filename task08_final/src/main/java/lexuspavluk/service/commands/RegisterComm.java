package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.beans.User;
import lexuspavluk.dao.GenericDAO;
import lexuspavluk.dao.sql.MySQLUsersDao;
import lexuspavluk.service.ActionCommand;
import lexuspavluk.service.LoginLogic;
import lexuspavluk.service.Messages;

import javax.servlet.http.HttpServletRequest;

public class RegisterComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request)  {
        String login = request.getParameter(AppConst.PARAM_LOGIN).trim();
        String password = request.getParameter(AppConst.PARAM_PASSWORD).trim();
        User newUser = new User(login, password);
        GenericDAO<User> userDAO = (MySQLUsersDao) factory.getDAO(newUser);
        User createdUser = userDAO.createRecord(newUser);
        userDAO.closeConnection();
        if (LoginLogic.checkLogin(createdUser) != -1) {
            request.setAttribute(AppConst.PARAM_LOGIN, login);
        } else {
            request.setAttribute("errorLoginPassMessage", Messages.REGISTRATION_ERROR);
        }
        return AppConst.INDEX_PAGE;
    }
}
