package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.beans.Tasks;
import lexuspavluk.service.ActionCommand;
import lexuspavluk.service.TablesLogic;

import javax.servlet.http.*;

public class TasksGetComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        Tasks tasks = new TablesLogic(request).getTasksTable();
        request.setAttribute(AppConst.PARAM_TASK_LIST, tasks);
        return AppConst.TASK_PAGE;
    }
}