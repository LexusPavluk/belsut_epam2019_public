package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.service.ActionCommand;

import javax.servlet.http.HttpServletRequest;

public class LogoutComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        return AppConst.INDEX_PAGE;
    }
}
