package lexuspavluk.service.commands;

import lexuspavluk.beans.Task;
import lexuspavluk.beans.User;
import lexuspavluk.dao.sql.MySQLTaskDao;
import lexuspavluk.service.ActionCommand;

import javax.servlet.http.HttpServletRequest;

public class FileUploadComm implements ActionCommand {
    private Integer ts_id;
    private String fileName;

    public FileUploadComm(Integer ts_id, String fileName) {
        this.ts_id = ts_id;
        this.fileName = fileName;
    }

    @Override
    public String execute(HttpServletRequest request) {
        Task task = new Task();

        MySQLTaskDao taskDAO = (MySQLTaskDao) factory.getDAO(task);
        task = taskDAO.getByPK(ts_id);
        task.setFilename(fileName);
        boolean isTaskUpdate = taskDAO.update(task);
        taskDAO.closeConnection();

        return "Your file is uploaded.";
                /*new TasksGetComm().execute(request)*/
    }

}