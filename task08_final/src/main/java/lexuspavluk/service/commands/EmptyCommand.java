package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.service.ActionCommand;

import javax.servlet.http.HttpServletRequest;

public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(AppConst.PARAM_USER) != null) {
            return AppConst.TASK_PAGE;
        } else {
            return AppConst.INDEX_PAGE;
        }
    }

}
