package lexuspavluk.service.commands;

import lexuspavluk.AppConst;
import lexuspavluk.beans.User;
import lexuspavluk.service.ActionCommand;
import lexuspavluk.service.LoginLogic;
import lexuspavluk.service.Messages;

import javax.servlet.http.HttpServletRequest;

public class LoginComm implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String login = request.getParameter(AppConst.PARAM_LOGIN).trim();
        String password = request.getParameter(AppConst.PARAM_PASSWORD).trim();
        User loginedUser = new User(login, password);
        int user_id = LoginLogic.checkLogin(loginedUser);
        if (user_id != -1) {
			User user = new User(user_id, login,null);
			request.getSession().setAttribute(AppConst.PARAM_USER, user);
            request.setAttribute(AppConst.PARAM_TABLE_TYPE, AppConst.TYPE_TODAY);
            page = new TasksGetComm().execute(request);
        } else {
            request.setAttribute("errorLoginPassMessage", Messages.LOGIN_ERROR);
            page = AppConst.INDEX_PAGE;
        }
        return page;
    }
}
