package lexuspavluk.service;

import lexuspavluk.AppConst;
import lexuspavluk.TablesGetEnum;
import lexuspavluk.beans.Task;
import lexuspavluk.beans.Tasks;
import lexuspavluk.beans.User;
import lexuspavluk.dao.sql.MySQLTaskDao;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static lexuspavluk.service.ActionCommand.factory;

public class TablesLogic {

    private final HttpServletRequest request;

    public TablesLogic(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    private TablesGetEnum defineTableType() {
        if (request.getParameter(AppConst.PARAM_TABLE_TYPE) != null) {
            return TablesGetEnum.valueOf(request.getParameter(AppConst.PARAM_TABLE_TYPE).toUpperCase());
        } else {
            return TablesGetEnum.TODAY;
        }
    }

    public Tasks getTasksTable () {
        User user = (User) request.getSession().getAttribute(AppConst.PARAM_USER);
        Integer ts_user_id = user.getId();
        TablesGetEnum tableType = defineTableType();
        MySQLTaskDao taskDAO = (MySQLTaskDao) factory.getDAO(new Task());
        List<Task> list = taskDAO.getAllByUserIdAndTableType(ts_user_id, tableType);
        taskDAO.closeConnection();
        return new Tasks(tableType.toString(), list);
    }
}