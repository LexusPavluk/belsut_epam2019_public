package lexuspavluk;

import lexuspavluk.service.ActionFactory;
import lexuspavluk.service.commands.UpdateTypesEnum;

public class AppConst {
    public static final String STORAGE_FOLDER_PATH  = "e:/data";

    // DB - values of column names
    public static final String COL_USER_ID = "id";
    public static final String COL_USER_LOGIN = "login";
    public static final String COL_USER_PASS = "password";

    public static final String COL_TASKS_ID = "id";
    public static final String COL_TASKS_NOTE = "ts_note";
    public static final String COL_TASKS_DATE = "ts_date";
    public static final String COL_TASKS_STATUS = "ts_status";
    public static final String COL_TASKS_FILENAME = "ts_filename";
    public static final String COL_TASKS_USER_KEY = "ts_user_id";

    // page's names
    public static final String ACTION_CONTROLLER = "/todo";
    public static final String INDEX_PAGE = "/index.jsp";
    public static final String TASK_PAGE = "/tasks.jsp";
    public static final String TASK_TABLE_PAGE = "/table.jsp";

    // command names
    public static final String KEY_LOGIN = ActionFactory.CommandEnum.LOGIN.toString().toLowerCase();
    public static final String KEY_LOGOUT = ActionFactory.CommandEnum.LOGOUT.toString().toLowerCase();
    public static final String KEY_REGISTRATION = ActionFactory.CommandEnum.REGISTRATION.toString().toLowerCase();
    public static final String KEY_NEW_TASK = ActionFactory.CommandEnum.NEW_TASK.toString().toLowerCase();
    public static final String KEY_GET_TASKS = ActionFactory.CommandEnum.GET_TASKS.toString().toLowerCase();
    public static final String KEY_UPDATE = ActionFactory.CommandEnum.UPDATE_TASK.toString().toLowerCase();
    public static final String KEY_DELETE = ActionFactory.CommandEnum.DELETE_TASK.toString().toLowerCase();
    public static final String KEY_DELETE_ALL = ActionFactory.CommandEnum.DELETE_ALL_TASK.toString().toLowerCase();
    public static final String KEY_FILE_PUSH = "FILE_PUSH".toLowerCase();
    public static final String KEY_FILE_GET = "FILE_GET".toLowerCase();



    // request attribute's names
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_PASSWORD = "pass";
	public static final String PARAM_USER = "user";
    public static final String PARAM_TASK_ID = "task_id";
    public static final String PARAM_ATTACHMENT = "attachment";
    public static final String PARAM_TABLE_TYPE = "table_type";
    public static final String PARAM_TASK_LIST = "task_list";


    // View tables types
    public static final String TYPE_TODAY = TablesGetEnum.TODAY.toString();
    public static final String TYPE_TOMORROW = TablesGetEnum.TOMORROW.toString();
    public static final String TYPE_SOMEDAY = TablesGetEnum.SOMEDAY.toString();
    public static final String TYPE_DONE = TablesGetEnum.DONE.toString();
    public static final String TYPE_RECYCLED = TablesGetEnum.RECYCLED.toString();

	// Types of UPDATE request
	public static final String UPD_LATER = UpdateTypesEnum.LATER.toString();
	public static final String UPD_DONE = UpdateTypesEnum.DONE.toString();
	public static final String UPD_RECYCLE = UpdateTypesEnum.RECYCLE.toString();
	public static final String UPD_RESTORE = UpdateTypesEnum.RESTORE.toString();
}
