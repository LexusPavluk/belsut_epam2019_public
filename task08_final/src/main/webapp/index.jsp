<%@ page import="lexuspavluk.AppConst" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
    <title>My ToDo!</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script-1.js"></script>
</head>

<body style="background: #868686">
    <div class="singup">
        <img src="img/25823707.jpg" alt="Back" style="width:100%">
        <h1>My ToDo list..</h1>
        <%
            String userLogin = AppConst.PARAM_LOGIN;
        if (request.getAttribute(userLogin) != null) {
        out.println("<p> Congratulations!!! User '" + request.getAttribute(userLogin) + "' created!</p>");
        }
        %>

        <button onclick="document.getElementById('id_login').style.display='block'; upModal()">Log in</button>
    </div>
    <div id="id_login" class="modal">
        <form class="modal-content animate" action="<%=AppConst.ACTION_CONTROLLER%>" method="post">
            <div class="imgcontainer">
                <img src="img/R2.png" alt="Аватар" class="avatar">
            </div>
            <div class="container">
                <label>
                    <input type="text" placeholder="login" name="login" pattern="[\w]{3,}" required>
                </label>
                <label>
                    <input type="password" placeholder="password" name="pass" pattern="[\w]{4,}" required>
                </label>
                <button type="submit" name="command" value="<%=AppConst.KEY_LOGIN%>">Log in</button>
                <button type="submit" name="command" value="<%=AppConst.KEY_REGISTRATION%>">Sign up</button>
            </div>
        </form>
    </div>
</body>
</html>