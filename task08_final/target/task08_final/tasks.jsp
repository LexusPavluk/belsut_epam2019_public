<%@ page import="lexuspavluk.AppConst" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!--<!DOCTYPE html>-->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <title>Todo List</title>
</head>

<body>
<c:set var="current_table_type" value="${requestScope.task_list.tableType}"/>
<!-- Top menu -->
<header class="topnav">
	<span style="float: left">Hello, ${sessionScope.user.login}. </span> <%---${table_type}---%>
    <form name="Logout" action="todo" method="post">
        <input type="hidden" name="command" value="logout"/>
        <button class="fa fa-sign-out" type="submit"> LOG OUT </button>
    </form>
</header>
<!--menu -->
<div class="bg">

    <c:set var="controller" value="<%=AppConst.ACTION_CONTROLLER%>"/>
    <c:set var="today" value="<%=AppConst.TYPE_TODAY%>"/>
    <c:set var="tomorrow" value="<%=AppConst.TYPE_TOMORROW%>"/>
    <c:set var="someday" value="<%=AppConst.TYPE_SOMEDAY%>"/>
    <c:set var="done" value="<%=AppConst.TYPE_DONE%>"/>
    <c:set var="recycled" value="<%=AppConst.TYPE_RECYCLED%>"/>

    <div id="navbar">
        <%--TABLE TYPE CHOICE--%>

        <button style="width: 10%; background: rgba(139,0,0,0.71)"
                onclick="document.getElementById('newTask').style.display='block'">NEW</button>

        <form action="${controller}" method="post">
            <input type="hidden" name="command" value="get_tasks">
            <input type="hidden" name="table_type" value="${today}">
            <button style="background: rgba(172,217,241,0.6)"
                    type="submit" name="table_type" >TODAY</button>
        </form>

        <form id="Tomorrow" action="${controller}" method="post">
            <input type="hidden" name="command" value="get_tasks">
            <input type="hidden" name="table_type" value="${tomorrow}">
            <button style="background: rgba(150, 190, 213, 0.6)"
                    type="submit" name="table_type" >TOMORROW</button>
        </form>

        <form id="Someday" action="${controller}" method="post">
            <input type="hidden" name="command" value="get_tasks">
            <input type="hidden" name="table_type" value="${someday}">
            <button style="background: rgba(84, 108, 110, 0.6)"
                    type="submit" name="table_type" >SOMEDAY</button>
        </form>

        <form id="Done" action="${controller}" method="post">
            <input type="hidden" name="command" value="get_tasks">
            <input type="hidden" name="table_type" value="${done}">
            <button style="background: rgba(227, 205, 148, 0.6)"
                    type="submit" name="table_type" >DONE</button>
        </form>

        <form id="Recycled" action="${controller}" method="post">
            <input type="hidden" name="command" value="get_tasks">
            <input type="hidden" name="table_type" value="${recycled}">
            <button type="submit" style="background: rgba(87,58,18,0.7)"
                    name="table_type" >RECYCLED</button>
        </form>
    </div>

    <div id="newTask" class="modal">
        <form class="modal-content animate" action="${controller}" method="post">
            <div class="container">
                <input type="hidden" name="command" value="<%=AppConst.KEY_NEW_TASK%>"/>
                <input type="hidden" name="table_type" value="${current_table_type}"/>
                <label>
                    <input type="text" size="64" maxlength="64" name="note" placeholder="Your note" required
                           style="width: 100%">
                </label>
                <label>
                    <input type="date" name="date" required>
                </label>
                <button type="submit" >NEW</button>
            </div>
            <div class="container" style="background-color:#f1f1f1">
                <button type="button" onclick="document.getElementById('newTask').style.display='none'"
                        class="cancelbtn">Cancel</button>
            </div>
        </form>
    </div>


    <div>
        <br/>
        <c:out value="${current_table_type}" />
        <c:choose>
            <c:when test="${current_table_type == someday}" >
                <c:forEach var="task" items="${requestScope.task_list.data}">
                    <div class="btn-group">
                        <button style="width:82%; font-size: 28px">${task.id}. ${task.note} </button>
                        <c:choose>
                            <c:when test="${not empty task.filename}">
                                <form title="${task.filename}" action="/upload" method="get">
                                    <input type="hidden" name="attachment" value="${task.filename}">
                                    <input type="hidden" name="task_id" value="${task.id}">
                                    <button class="fa fa-file" type="submit"></button>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <button class="fa fa-file-o myBtn"
                                        onclick="document.getElementById('myModal${task.id}').style.display='block'"></button>
                            </c:otherwise>
                        </c:choose>

                        <div id="myModal${task.id}" class="modal">
                            <form id="sendForm" class="modal-content animate" action="${controller}" method="post" enctype="multipart/form-data">
                                <div class="container">
                                    ${task.id}
                                    <input type="hidden" name="table_type" value="${current_table_type}"/>
                                    <input type="hidden" name="task_id" value="${task.id}">
                                    <input type="file" name="attachment"/> <br>
                                    <input type="submit" name="Upload"/>
                                </div>
                                <div class="container" style="background-color:#f1f1f1">
                                    <button type="button" onclick="document.getElementById('myModal${task.id}').style.display='none'"
                                            class="cancelbtn">Cancel</button>
                                </div>
                            </form>
                        </div>

                        <form action="${controller}" method="post">
                            <input type="hidden" name="command" value="<%=AppConst.KEY_UPDATE%>"/>
                            <input type="hidden" name="table_type" value="${current_table_type}"/>
                            <input type="hidden" name="task_id" value="${task.id}">
                            <input type="hidden" name="task_note" value="${task.note}">
                            <input type="hidden" name="task_date" value="${task.date}">
                            <input type="hidden" name="task_status" value="${task.status}">
                            <input type="hidden" name="task_file" value="${task.filename}">

                            <button class="fa fa-check-square-o"  type="submit" name="update"
                                    value="<%=AppConst.UPD_DONE%>" ></button>

                            <button class="fa fa-trash"  type="submit" name="update"
                                    value="<%=AppConst.UPD_RECYCLE%>" ></button>
                        </form>
                    </div>

                </c:forEach>
            </c:when>

            <c:when test="${current_table_type == done}">
                <c:forEach var="task" items="${requestScope.task_list.data}">
                    <div class="btn-group">
                        <button style="width:82%; font-size: 28px">${task.id}. ${task.note} </button>
                        <c:choose>
                            <c:when test="${not empty task.filename}">
                                <form title="${task.filename}" action="/upload" method="get">
                                    <input type="hidden" name="attachment" value="${task.filename}">
                                    <input type="hidden" name="task_id" value="${task.id}">
                                    <button class="fa fa-file" type="submit"></button>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <button class="fa fa-file-o myBtn"></button>
                            </c:otherwise>
                        </c:choose>

                        <div id="myModal${task.id}" class="modal">
                            <form id="sendForm" class="modal-content animate" action="${controller}" method="post" enctype="multipart/form-data">
                                <div class="container">
                                    <input type="hidden" name="table_type" value="${current_table_type}"/>
                                    ${task.id}
                                    <input type="hidden" name="task_id" value="${task.id}">
                                    <input type="file" name="attachment"/> <br>
                                    <input type="submit" name="Upload"/>
                                </div>
                                <div class="container" style="background-color:#f1f1f1">
                                    <button type="button" onclick="document.getElementById('myModal${task.id}').style.display='none'"
                                            class="cancelbtn">Cancel</button>
                                </div>
                            </form>
                        </div>

                        <form action="${controller}" method="post">
                            <input type="hidden" name="command" value="<%=AppConst.KEY_UPDATE%>"/>
                            <input type="hidden" name="table_type" value="${current_table_type}"/>
                            <input type="hidden" name="task_id" value="${task.id}">
                            <input type="hidden" name="task_note" value="${task.note}">
                            <input type="hidden" name="task_date" value="${task.date}">
                            <input type="hidden" name="task_status" value="${task.status}">
                            <input type="hidden" name="task_file" value="${task.filename}">

                            <button class="fa fa-repeat"  type="submit" name="update"
                                    value="<%=AppConst.UPD_RESTORE%>" ></button>

                            <button class="fa fa-trash"  type="submit" name="update"
                                    value="<%=AppConst.UPD_RECYCLE%>" ></button>
                        </form>
                    </div>
                </c:forEach>
            </c:when>

            <c:when test="${current_table_type == recycled}">
                <form class="btn-group" action="${controller}" method="post">
                    <input type="hidden" name="table_type" value="${recycled}">
                    <input type="hidden" name="command" value="<%=AppConst.KEY_DELETE_ALL%>"/>
                    <button class="fa fa-trash" style="width: 100%; background: rgba(176,0,0,0.72)"
                            type="submit"> CLEAR RECYCLE (DELETE ALL) </button>
                </form>
                <c:forEach items="${requestScope.task_list.data}" var="task">
                    <div class="btn-group">
                        <button style="width:82%; font-size: 28px">${task.id}. ${task.note} </button>

                            <c:choose>
                                <c:when test="${not empty task.filename}">
                                    <form title="${task.filename}" action="/upload" method="get">
                                        <input type="hidden" name="attachment" value="${task.filename}">
                                        <input type="hidden" name="task_id" value="${task.id}">
                                        <button class="fa fa-file" type="submit"></button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <button class="fa fa-file-o myBtn"></button>
                                </c:otherwise>
                            </c:choose>

                        <form action="${controller}" method="post"
                              style="background: rgba(150, 190, 213, 0.6)">
                            <input type="hidden" name="command" value="<%=AppConst.KEY_UPDATE%>"/>
                            <input type="hidden" name="task_id" value="${task.id}">
                            <input type="hidden" name="task_note" value="${task.note}">
                            <input type="hidden" name="task_date" value="${task.date}">
                            <input type="hidden" name="task_status" value="${task.status}">
                            <input type="hidden" name="task_file" value="${task.filename}">
                            <button class="fa fa-repeat"  type="submit" name="update"
                                    value="<%=AppConst.UPD_RESTORE%>" ></button>
                        </form>
                        <form action="${controller}" method="post">
                            <input type="hidden" name="command" value="<%=AppConst.KEY_DELETE%>"/>
                            <input type="hidden" name="table_type" value="${current_table_type}"/>
                            <input type="hidden" name="task_id" value="${task.id}">
                            <button class="fa fa-window-close" style="background: #b00000" type="submit"></button>
                        </form>
                    </div>
                </c:forEach>
            </c:when>

            <c:otherwise>
                <c:forEach var="task" items="${requestScope.task_list.data}">
                    <div class="btn-group">
                            <button style="width:76%; font-size: 28px">${task.id}. ${task.note} </button>
                            <c:choose>
                                <c:when test="${not empty task.filename}">
                                    <form title="${task.filename}" action="/upload" method="get">
                                        <input type="hidden" name="attachment" value="${task.filename}">
                                        <input type="hidden" name="task_id" value="${task.id}">
                                        <button class="fa fa-file" type="submit"></button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <button class="fa fa-file-o myBtn"
                                            onclick="document.getElementById('myModal${task.id}').style.display='block'"></button>
                                </c:otherwise>
                            </c:choose>

                            <div id="myModal${task.id}" class="modal">
                                <form class="modal-content animate" action="${controller}" method="post" enctype="multipart/form-data">
                                    <div class="container">
                                        <input type="hidden" name="table_type" value="${current_table_type}"/>
                                        ${task.id}
                                        <input type="hidden" name="task_id" value="${task.id}">
                                        <input type="file" name="attachment"/> <br>
                                            <input type="submit" name="Upload"/>
                                    </div>
                                    <div class="container" style="background-color:#f1f1f1">
                                        <button type="button" onclick="document.getElementById('myModal${task.id}').style.display='none'"
                                                class="cancelbtn">Cancel</button>
                                    </div>
                                </form>
                            </div>

                        <form action="${controller}" method="post">
                            <input type="hidden" name="command" value="<%=AppConst.KEY_UPDATE%>"/>
                            <input type="hidden" name="table_type" value="${current_table_type}"/>
                            <input type="hidden" name="task_id" value="${task.id}">
                            <input type="hidden" name="task_note" value="${task.note}">
                            <input type="hidden" name="task_date" value="${task.date}">
                            <input type="hidden" name="task_status" value="${task.status}">
                            <input type="hidden" name="task_file" value="${task.filename}">

                            <button class="fa fa-chevron-right"  type="submit" name="update"
                                    value="<%=AppConst.UPD_LATER%>" ></button>

                            <button class="fa fa-check-square-o"  type="submit" name="update"
                                    value="<%=AppConst.UPD_DONE%>" ></button>

                            <button class="fa fa-trash"  type="submit" name="update"
                                    value="<%=AppConst.UPD_RECYCLE%>" ></button>
                        </form>
                    </div>

                </c:forEach>
            </c:otherwise>
        </c:choose>


    </div>
</div>

<script>
    window.onscroll = function () {
        myFunction()
    };
    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }

</script>

</body>
</html>