<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="lexuspavluk.AppConst" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
        <link rel="stylesheet" href="css/style.css">
        <title>Uploaded</title>
    </head>

    <body style="background: #868686">
        <c:set var="controller" value="<%=AppConst.ACTION_CONTROLLER%>"/>
        <form class="singup" action="${controller}" method="post">
            <h2>${requestScope.message}</h2>
            <input type="hidden" name="command" value="get_tasks">
            <input type="hidden" name="table_type" value="TODAY">
            <button style="background: rgba(172,217,241,0.6)"
                    type="submit" name="table_type" >RETURN</button>
        </form>

    </body>
</html>
