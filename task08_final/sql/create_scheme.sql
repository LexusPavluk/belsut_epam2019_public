create table users
(
    id       int(11) unsigned auto_increment,
    login    varchar(45) not null,
    password varchar(45) not null,
    constraint idusers_UNIQUE
        unique (id),
    constraint login_UNIQUE
        unique (login)
)
    comment 'nv';

alter table users
    add primary key (id);

create table tasks
(
    id          int(11) unsigned auto_increment,
    ts_note     varchar(100)                                     not null,
    ts_date     date                                             not null,
    ts_status   enum ('TODO', 'DONE', 'RECYCLED') default 'TODO' not null,
    ts_filename varchar(256)                                     null,
    ts_user_id  int(11) unsigned                                 not null,
    constraint idtodo_UNIQUE
        unique (id),
    constraint tasks_users_us_id_fk
        foreign key (ts_user_id) references users (id)
            on update cascade on delete cascade
);

alter table tasks
    add primary key (id);

