import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Main {
    static final String ADDITION = "ADC";
    static final String SOURCE_DIR = "ADC\\";
    static StringBuilder posCode = new StringBuilder();
    static StringBuilder newPOScode = new StringBuilder();

    // задаём значения POScode
    static void setPOScodes(File file) {
        String oldName = file.getName();
        posCode = new StringBuilder(oldName.substring(oldName.indexOf('_') + 1, oldName.indexOf('.')));
        newPOScode = new StringBuilder(ADDITION + "_" + posCode);
    }

    public static void main(String[] args) throws IOException {
        File currentDirectory = new File(SOURCE_DIR);
        File[] files = currentDirectory.listFiles();
        fileAction(files);
    }

    // обработка файлов и каталогов
    public static void fileAction(File[] files) {
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                fileAction(Objects.requireNonNull(files[i].listFiles()));
            }
            if (!files[i].getName().contains(ADDITION)) {
                if (files[i].isFile()) {
                    setPOScodes(files[i]);
                    new ModifyXMLFile(files[i].getPath(), new String(newPOScode)).modifyXMLElements();
                }
                renameFile(files[i]);
            }
        }
    }

    //переименование файлов и директорий
    private static void renameFile(File file) {
        File destination = new File(setDestinationADCPath(file));
        System.out.println(destination.toString());
        String notRenamed = "File " + file + " don't renamed!!!";
        System.out.println(file.renameTo(destination) ? ""/*renamed*/ : notRenamed);
    }

    // задаём целевой путь переименования файла
    static String setDestinationADCPath(File file) {
        if (file.isFile()) {
            return file.getParent() + File.separator + file.getName().replace(posCode, newPOScode);
        } else {
            return file.getParent() + File.separator + ADDITION + "_" + file.getName();
        }
    }
}
