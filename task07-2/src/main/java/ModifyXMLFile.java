import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class ModifyXMLFile {
    String filePath;
    String newPosCode;

    public ModifyXMLFile(String filePath, String newCode) {
        this.filePath = filePath;
        this.newPosCode = newCode;
    }

    public void modifyXMLElements() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filePath);

            String parentPOSvalue = newPosCode.substring(0, newPosCode.lastIndexOf('_'));
            setAttrValue(doc, "PointOfSale", "ParentPointOfSale", parentPOSvalue);
            setAttrValue(doc, "PointOfSale", "PointOfSaleCode", newPosCode);
            setAttrValue(doc, "PointOfSaleDescription", "Description", newPosCode);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult((new File(filePath)));
            transformer.transform(source, result);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private void setAttrValue(Document doc, String node, String attrName, String newAttrValue) {
        Node thisNode = doc.getElementsByTagName(node).item(0);
        NamedNodeMap attr = thisNode.getAttributes();
        Node attribute = attr.getNamedItem(attrName);
        attribute.setTextContent(newAttrValue);
    }

}
