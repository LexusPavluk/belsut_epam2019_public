import org.apache.commons.io.filefilter.WildcardFileFilter;
import java.io.File;
import java.io.IOException;

public class FSActions {

    public static void readFileNames(String path) throws IOException {
        File currentDirectory = new File(path);    // текущий каталог
        String[] files = currentDirectory.list();  // список файлов , каталогов
        String[] filesNames = currentDirectory.list( new WildcardFileFilter("ADS*") );  // список файлов по шаблону
    }
    public static void usingWildcardFileFilter(String path) throws IOException {
    }
}
