package oop_first_lab.entities;

/** Marker interface - describes the ability of an entity to be transparent.
 *
 *  @author Alexey Pavlyuchenkov
 *  @author email: lexuspavluk@gmail.com
 *  @version 1.0
 */

public interface ITransparentable {
}
