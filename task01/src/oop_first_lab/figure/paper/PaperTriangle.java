package oop_first_lab.figure.paper;

import oop_first_lab.figure.*;
import java.util.Objects;

/**
 * PaperTriangle class - extend of abstract class <tt>Triangle</tt>.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public class PaperTriangle extends Triangle implements IPaper {

    /**
     * The {@code Colors} value of
     * <i>paperColor</i> from <tt>PaperTriangle</tt>. Field paperColor is reference in nested <tt>Color</tt> object,
     * which stores value of paper color.
     */
    private Painted paperColor = new Painted();

    /**
     * Constructor of class PaperTriangle with specified side.
     *
     * @param side - side of isosceles PaperTriangle.
     */
    public PaperTriangle(double side) {
        super(side);
    }

    /**
     * Constructor of class PaperTriangle with specified side, and the <tt>Figure</tt> from which it is cut.
     *
     * @param side   - side of isosceles PaperTriangle.
     * @param figure - Figure which cut.
     */
    public PaperTriangle(double side, IPaper figure) {
        super(side, (Figure) figure);
        this.paperColor.setColor(figure.getColorSheet());
    }

    @Override
    public Palette getColorSheet() {
        return paperColor.getColor();
    }

    @Override
    public void setColorSheet(Palette newColor) {
        this.paperColor.setColor(newColor);
    }

    @Override
    public String toString() {
        return super.toString() + " " + getColorSheet();
    }

    public boolean equals(Object obj) {
        if(super.equals(obj)) {
            PaperTriangle other = (PaperTriangle) obj;
            return getColorSheet() == other.getColorSheet();
        } else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), paperColor);
    }

}
