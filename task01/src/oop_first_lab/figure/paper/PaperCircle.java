package oop_first_lab.figure.paper;

import oop_first_lab.figure.Circle;
import oop_first_lab.figure.Figure;

import java.util.Objects;

/**
 * PaperCircle class - extend abstract class <tt>Circle</tt>, implements interface <tt>IPaintable</tt> - can be Painted.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public class PaperCircle extends Circle implements IPaper {

    /**
     * The {@code Colors} value of
     * <i>paperColor</i> from <tt>PaperCircle</tt>. Field paperColor is reference in nested <tt>Color</tt> object,
     * which stores value of paper color.
     */
    Painted paperColor = new Painted();

    /**
     * Constructor of class <tt>PaperCircle</tt> with specified radius.
     *
     * @param radius radius for making <tt>PaperCircle</tt>.
     */
    public PaperCircle(double radius) {
        super(radius);
    }

    /**
     * Constructor of class <tt>PaperCircle</tt> with specified radius, and the <tt>Figure</tt> from which it is cut.
     *
     * @param radius radius for making <tt>PaperCircle</tt>.
     * @param figure <tt>Figure</tt> which cut.
     */
    public PaperCircle(double radius, IPaper figure) {
        super(radius, (Figure) figure);
        this.paperColor.setColor(figure.getColorSheet());
    }

    @Override
    public Palette getColorSheet() {
        return paperColor.getColor();
    }

    @Override
    public void setColorSheet(Palette newColor) {
        this.paperColor.setColor(newColor);
    }

    @Override
    public String toString() {
        return super.toString() + " " + getColorSheet();
    }

    @Override
    public boolean equals(Object obj) {
        if(super.equals(obj)) {
            PaperCircle other = (PaperCircle) obj;
            return getColorSheet() == other.getColorSheet();
        } else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), paperColor);
    }
}
