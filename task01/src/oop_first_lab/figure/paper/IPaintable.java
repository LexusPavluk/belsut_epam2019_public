package oop_first_lab.figure.paper;

/**
 * The marker interface describes "Painting" property and "Color identifications"
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */

public interface IPaintable {

    enum Palette {
        RED,
        ROSE,
        ORANGE,
        YELLOW,
        GREEN,
        BLUE,
        VIOLET,
        BLACK,
        WHITE
    }


    /**
     * Inner class <tt>Color</tt> for storing value of paper color.
     */
    public class Painted {

        /**
         * The {@code Colors} value of
         * <i>color</i> .
         */
        private Palette color;

        /**
         * Default constructor of class <tt>Color</tt> sets the color value WHITE.
         */
        public Painted() {
            this.color = Palette.WHITE;
        }

        /**
         * Painting object.
         *
         * @param newColor - set color of object for <tt>Colors</tt>.
         * @see Palette
         */
        public void setColor(Palette newColor) {
            if (color != Palette.WHITE) {
                //System.out.println("It's already painted");
            } else {
                color = newColor;
            }

        }

        /**
         * Color identification.
         *
         * @return color of object.
         * @see Palette
         */
        public Palette getColor() {
            return color;
        }

    }


}
