package oop_first_lab.figure.paper;

import oop_first_lab.entities.ISheet;

/**
 * Marker interface of Paper - description of the Painted Sheet
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */

public interface IPaper extends ISheet, IPaintable {

    /**
     * Determining the color of the sheet.
     *
     * @return color of object.
     * @see Palette
     */
    public Palette getColorSheet();

    /**
     * Painting sheet.
     *
     * @param newColor - set color of sheet for <tt>Palette</tt>.
     * @see Palette
     */
    public void setColorSheet(Palette newColor);
}
