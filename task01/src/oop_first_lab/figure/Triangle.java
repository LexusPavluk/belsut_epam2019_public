package oop_first_lab.figure;

import java.util.Objects;
import static java.lang.Math.*;

/**
 * Abstract class of Triangle - implements methods of abstract class <tt> Figure </tt>, from which extended.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */
public abstract class Triangle extends Figure {

    /**
     * The {@code double} value of
     * <i>SIDE</i> from isosceles Triangle.
     */
    private final double side;

    /**
     * Constructor of class Triangle with specified side.
     * @param side - side of isosceles Triangle.
     */
    public Triangle(double side) {
        ValidationUtils.verificationPositivityNumber(side);
        this.side = side;
    }

    /**
     * Constructor of class Triangle with specified side, and the <tt>Figure</tt> from which it is cut.
     * @param side - side of isosceles Triangle.
     * @param figure - Figure which cut.
     */
    public Triangle(double side, Figure figure){
        ValidationUtils.verificationPositivityNumber(side);
        ValidationUtils.checkPossibilityMakeFigure(figure, side);
        this.side = side;
    }

    /**
     * Getter for side of Triangle.
     * @return SIDE - side of Triangle.
     */
    public double getSide() {
        return side;
    }

    @Override
    public double getArea() {
        return sqrt(3.0 / 16) * pow(side, 2);
    }

    @Override
    public double getPerimeter() {
        return side * 3;
    }

    @Override
    public double getDefiningSize() {
        return side / (2 * sqrt(3.0));
    } //defining size is side divide by twice root of 3

    @Override
    public String toString() {
        return super.toString() + " " + side;
    }

    @Override
    public boolean equals(Object obj) {
        if(super.equals(obj)) {
            Triangle other = (Triangle) obj;
            return Double.compare(side, other.getSide()) == 0;
        } else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), side);
    }

}
