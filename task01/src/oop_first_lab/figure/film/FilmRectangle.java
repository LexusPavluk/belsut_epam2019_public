package oop_first_lab.figure.film;

import oop_first_lab.figure.*;

/**
 * Film Rectangle class - extend abstract class <tt>Rectangle</tt>.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 */

public class FilmRectangle extends Rectangle implements IFilm {

     /**
     * Constructor of class FilmRectangle with specified sides.
     * @param side1 first side for making FilmRectangle.
     * @param side2 second side for making FilmRectangle.
     */
    public FilmRectangle(double side1, double side2) {
        super(side1, side2);
    }

    /**
     * Constructor of class FilmRectangle with specified sides, and the <tt>Figure</tt> from which it is cut.
     * @param side1 first side for making FilmRectangle.
     * @param side2 second side for making FilmRectangle.
     * @param figure Figure which cut.
     */
    public FilmRectangle(double side1, double side2, IFilm figure) {
        super(side1, side2, (Figure) figure);
    }

}
