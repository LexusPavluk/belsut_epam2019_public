package oop_first_lab.unused;

public enum Shapes {
    OVAL("Oval"),
    CIRCLE("Circle"),
    TRIANGLE("Triangle"),
    RECTANGLE("Rectangle"),
    SQUARE("Square");

    String value;

    Shapes(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public double defineSquare (double ...dim) {
        switch (this) {
            case OVAL:
                return Math.PI * dim[0] * dim[1];
            case CIRCLE:
                return Math.PI * dim[0] * dim[0];
            case TRIANGLE:
                return 0.5 * dim[1] * dim[2];
            case RECTANGLE:
                return dim[0] * dim[1];
            case SQUARE:
                return dim[0] * dim[0];
            default: throw new EnumConstantNotPresentException(this.getDeclaringClass(), this.name());
        }
    }
}
