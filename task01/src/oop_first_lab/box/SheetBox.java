package oop_first_lab.box;

import oop_first_lab.entities.ISheet;
import oop_first_lab.figure.Circle;
import oop_first_lab.figure.Figure;
import oop_first_lab.figure.film.IFilm;
import java.util.ArrayList;
import java.util.List;

/**
 * Container class for figures sheets.
 *
 * @author Alexey Pavlyuchenkov
 * @author email: lexuspavluk@gmail.com
 * @version 1.0
 *
 */

public class SheetBox {
    private List<ISheet> container = new ArrayList<>();

    /**
     * Checking input data of correct values. Index of figure must be positive,
     * and less than total quantity of sheets.
     * @param  index - checking value.
     */
    private boolean correctIndexValue(int index) {
        return (index >= 0 &
                index < container.size());
    }
    /**
     * Adding one sheet to the box.
     * @param  figure - added sheet.
     */
    private boolean addFigure(ISheet figure) {
        if (!container.contains(figure) && figure != null) {
            container.add(figure);
            return true;
        }
        return false;
    }

    /**
     * Adding one and more different type sheets to the box.
     * @param  figures - added paper sheets.
     */
    public <T extends ISheet> void addFigure(T... figures) {
        for (int i = 0; i < figures.length; i++) {
            addFigure((ISheet) figures[i]);
        }
    }


    /**
     * Getting one sheet of the box by number.
     * @param  num - geted sheet.
     */
    public ISheet getByNumber(int num) {
        if(correctIndexValue(num)) {
            return container.get(num);
        } throw new IllegalArgumentException("Incorrect index value");
    }

    /**
     * Remove one sheet out of the box by number.
     * @param  num - removed sheet.
     */
    public boolean removeByNumber(int num) {
        if(correctIndexValue(num) &&
                container.get(num) != null) {
                container.remove(num);
                return true;
        } else return false;
    }

    /**
     * Changing of one sheet to another in box by number.
     * @param setFigure - changed sheet
     * @param  num - index location of another sheet.
     */
    public ISheet changeByNumber(ISheet setFigure, int num) {
        ISheet gettingISheet = null;
        if(correctIndexValue(num)) {
                gettingISheet = getByNumber(num);
                container.set(num, setFigure);
                return gettingISheet;
        }
        return gettingISheet;
    }

    /**
     * Number of locations specified sheet in the box.
     * @param figure - specified sheet.
     */
    public int indexPerSample(ISheet figure) {
            return container.indexOf(figure);
    }

    /**
     * Total quantity of sheets in the box.
     */
    public int getCountFigures() {
        return container.size();
    }

    /**
     * Total sheet's area in the box.
     */
    public double getTotalArea() {
        double totalArea = 0.0;
        for (ISheet elem : container) {
            totalArea += ((Figure) elem).getArea();
        }
        return totalArea;
    }

    /**
     * Total sheet's perimeter in the box.
     */
    public double totalPerimeter() {
        double totalPerimeter = 0.0;
        for (ISheet elem : container) {
            totalPerimeter += ((Figure)elem).getPerimeter();
        }
        return totalPerimeter;
    }

    /**
     * Getting all the Circles from the box. At the same time, circles putting in another container.
     */
    public void takeAllCircle(SheetBox otherBox) {
        for (int i = 0; i < container.size();) {
            if (container.get(i) instanceof Circle) {
                otherBox.addFigure(getByNumber(i));
                removeByNumber(i);
            } else i++;
        }
    }

    /**
     * Getting all the Film sheets from the box. At the same time, film sheets putting in another container.
     */
    public void takeAllFilmFigures(SheetBox otherBox) {
        for (int i = 0; i < container.size(); ) {
            if (container.get(i) instanceof IFilm) {
                otherBox.addFigure(getByNumber(i));
                removeByNumber(i);
            } else i++;
        }
    }

    @Override
    public String toString() {
        return "\n Box for the sheets {"
                + container +
                '}';
    }
}
