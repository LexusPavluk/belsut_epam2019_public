package collections;

import java.util.*;
import static collections.AlgorithmJohnsonsUtils.*;

public class Main {
    private static final String FILE_NAME = "resources\\test3.txt";

    public static void main(String[] args) {
        // создаем List процессов из файла
        List<WorkProcess> processList = FileToList.numbersFromFileToList(FILE_NAME);
        System.out.println(processList);
        // отбираем процессы с меньшим (или равным второму) первым временем
        List<WorkProcess> firstList = findProcessFastestInFirstMachine(processList);
        // отбираем процессы с меньшим вторым временем
        List<WorkProcess> secondList = new ArrayList<>(processList);
        secondList.removeAll(firstList);
        // сортируем наши наборы процессов согласно выбранным Компараторам
        firstList.sort(new t1Comparator());
        secondList.sort(new t2Comparator());
        // заносим в ощищенный первоначальный список отсортированные по алгоритму Джонсона наборы процессов
        processList.clear();
        processList.addAll(firstList);
        processList.addAll(secondList);
        System.out.println(processList);
        // выводим общее время процессов
        System.out.println(getTimeInCriticalRoute(processList));
    }
}
