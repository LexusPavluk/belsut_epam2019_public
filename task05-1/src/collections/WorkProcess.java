package collections;

public class WorkProcess {
    private int number;
    private int t1;
    private int t2;

    public WorkProcess(int numberProcess, int t1, int t2) {
        this.number = setPositiveValue(numberProcess);
        this.t1 = setPositiveValue(t1);
        this.t2 = setPositiveValue(t2);
    }

/*
    private void setNumberProcess(int numberProcess) {
        this.number = setPositiveValue(numberProcess);
    }

    private void setT1(int t1) {
        this.t1 = setPositiveValue(t1);
    }

    private void setT2(int t2) {
        this.t2 = setPositiveValue(t2);
    }
*/

    public int getNumber() {
        return number;
    }

    public int getT1() {
        return t1;
    }

    public int getT2() {
        return t2;
    }

    private static int setPositiveValue(int number) {
        if(number > 0){
            return number;
        } else throw new IllegalArgumentException("Value is less than 0.");
    }

    @Override
    public String toString() {
        return number +"(" + t1 + "," + t2 +")\n";
    }
}
