package collections;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AlgorithmJohnsonsUtils {

    private AlgorithmJohnsonsUtils() {
    }

    public static List<WorkProcess> findProcessFastestInFirstMachine(List<WorkProcess> list) {
        List<WorkProcess> firstList = new ArrayList<>();
        for (WorkProcess process : list) {
            if (process.getT1() <= process.getT2()) firstList.add(process);
        }
        return firstList;
    }

/*
    public static List<WorkProcess> findProcessFastestInSecondMachine(List<WorkProcess> list) {
        List<WorkProcess> secondList = new ArrayList<>(list);
        for (WorkProcess process : list) {
            if (process.getT1() > process.getT2()) secondList.add(process);
        }
        return secondList;
    }
*/

    public static class t1Comparator implements Comparator<WorkProcess> {

        @Override
        public int compare(WorkProcess o1, WorkProcess o2) {
            return o1.getT1() - o2.getT1();
        }
    }

    public static class t2Comparator implements Comparator<WorkProcess> {

        @Override
        public int compare(WorkProcess o1, WorkProcess o2) {
            return o2.getT2() - o1.getT2();
        }
    }

    public static int getTimeInCriticalRoute (List<WorkProcess> list) {
        int time = 0;
        for (WorkProcess process: list) {
            time += process.getT1();
        }
        time += list.get(list.size()-1).getT2();
        return time;
    }
}
