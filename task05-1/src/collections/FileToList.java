package collections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class FileToList {

    private FileToList() {
    }

    public static List<WorkProcess> numbersFromFileToList(String filename) {
        List<WorkProcess> processList = new ArrayList<>();
        try (Scanner scanFile = new Scanner(new File(filename))) {
            while (scanFile.hasNextLine()) {
                Scanner scanLine = new Scanner(scanFile.nextLine()).useDelimiter("\\D+");
                int numOfNums = 3; // количество чисел, характеризующее рабочий процесс
                int[] temp = new int[numOfNums];
                for (int i = 0; i < numOfNums; i++) {
                    temp[i] = Integer.parseInt(scanLine.next());
                }
                scanLine.close();
                processList.add(new WorkProcess(temp[0], temp[1], temp[2]));
            }
            scanFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return processList;
    }
}



